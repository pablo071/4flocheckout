<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arComponentDescription = array(
    "NAME" => GetMessage("CHECKOUT_NAME"),
    "DESCRIPTION" => GetMessage("CHECKOUT_DESCRIPTION"),
    "ICON" => "/images/icon.gif",
    "PATH" => array(
        "ID" => "e-store",
        "CHILD" => array(
            "ID" => "checkout",
            "NAME" => GetMessage("CHECKOUT_CHILD_NAME")
        )
    ),
);