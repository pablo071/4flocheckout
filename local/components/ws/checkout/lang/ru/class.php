<?
$MESS["CH_LOCATION_TYPE_CITY"] = "г. ";
$MESS["CH_COURIER_NAME"] = "Курьер";
$MESS["CH_COURIER_DESCRIPTION"] = "служба доставки";
$MESS["CH_PVZ_NAME"] = "Самовывоз";
$MESS["CH_PVZ_DESCRIPTION"] = "пункт выдачи";
$MESS["CH_DELIVERY_COST_FREE"] = "бесплатно";
$MESS["CH_DELIVERY_COST_FROM"] = "от #COST# &#8381;";
$MESS["CH_CAPTION_FIELD_NAME"] = "Имя";
$MESS["CH_CAPTION_FIELD_LAST_NAME"] = "Фамилия";
$MESS["CH_CAPTION_FIELD_SECOND_NAME"] = "Может потребоваться для некоторых видов доставки";
$MESS["CH_CAPTION_FIELD_EMAIL"] = "На этот адрес придет электронный чек";
$MESS["CH_CAPTION_FIELD_PHONE"] = "Номер телефона";
$MESS['USER_ALREADY_EXISTS_MESS'] = 'Пользователь с введенным электронным адресом уже зарегистирован, пожалуйста, авторизуйтесь.';
$MESS['DELIVERY_NOT_CHOOSE'] = 'Не выбрана';