<?php

namespace WS\Components;
IncludeTemplateLangFile(__FILE__);

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Sale\Basket;
use Bitrix\Sale\BasketItem;
use Bitrix\Sale\Delivery\Services\Manager;
use Bitrix\Sale\DiscountCouponsManager;
use Bitrix\Sale\Order;
use Bitrix\Sale\Payment;
use Bitrix\Sale\PaySystem;
use Bitrix\Sale\PropertyValue;
use Bitrix\Sale\Shipment;
use Bitrix\Sale\ShipmentItem;
use CFile;
use CIBlockElement;
use CUser;
use Verifier\Verifier;
use WS\Services\Money;
use WS\Tools\Module;

class Checkout extends \CBitrixComponent {

    const ID_PERSON_TYPE_INDIVIDUAL = 1;

    const IMAGE_SIZE_WIDTH = 64;
    const IMAGE_SIZE_HEIGHT = 64;

    const IMAGE_GIFT_SIZE_WIDTH = 111;
    const IMAGE_GIFT_SIZE_HEIGHT = 167;

    const ACTION_CREATE_ORDER = 'createOrder';

    const DELIVERY_ID_MOSCOW = '2';
    const DELIVERY_ID_OUT_MKAD = '3';
    const DELIVERY_ID_PICKUP = '4';

    const PROPERTY_CODE_FIO = 'FIO';
    const PROPERTY_CODE_NAME = 'NAME';
    const PROPERTY_CODE_LAST_NAME = 'LAST_NAME';
    const PROPERTY_CODE_PHONE = 'PHONE';
    const PROPERTY_CODE_EMAIL = 'EMAIL';
    const PROPERTY_CODE_NOT_CALL_ME = 'NOT_CALL_ME';
    const PROPERTY_CODE_RECEIVE_PERSONALLY = 'RECEIVE_PERSONALLY';
    const PROPERTY_CODE_RECIPIENT_NAME = 'RECIPIENT_NAME';
    const PROPERTY_CODE_RECIPIENT_LAST_NAME = 'RECIPIENT_LAST_NAME';
    const PROPERTY_CODE_RECIPIENT_PHONE = 'RECIPIENT_PHONE';
    const PROPERTY_CODE_ADD_FREE_POSTCARD = 'ADD_FREE_POSTCARD';
    const PROPERTY_CODE_TEXT_FREE_POSTCARD = 'TEXT_FREE_POSTCARD';
    const PROPERTY_CODE_ADD_DESIGN_POSTCARD = 'ADD_DESIGN_POSTCARD';
    const PROPERTY_CODE_TEXT_DESIGN_POSTCARD = 'TEXT_DESIGN_POSTCARD';
    const PROPERTY_CODE_HAND_OVER_INCOGNITO = 'HAND_OVER_INCOGNITO';
    const PROPERTY_CODE_PHOTO_WITH_RECIPIENT = 'PHOTO_WITH_RECIPIENT';
    const PROPERTY_CODE_SURPRISE = 'SURPRISE';
    const PROPERTY_CODE_NOT_CALL_ADVANCE = 'NOT_CALL_ADVANCE';
    const PROPERTY_CODE_NOT_KNOW_RECIPIENT_PHONE = 'NOT_KNOW_RECIPIENT_PHONE';
    const PROPERTY_CODE_ASK_RECIPIENT_ADDRESS = 'ASK_RECIPIENT_ADDRESS';
    const PROPERTY_CODE_CITY = 'CITY';
    const PROPERTY_CODE_STREET = 'STREET';
    const PROPERTY_CODE_HOUSE = 'HOUSE';
    const PROPERTY_CODE_BUILDING = 'BUILDING';
    const PROPERTY_CODE_FLAT = 'FLAT';
    const PROPERTY_CODE_PRIVATE_HOUSE = 'PRIVATE_HOUSE';
    const PROPERTY_CODE_PICKUP_ID = 'PICKUP_ID';
    const PROPERTY_CODE_PICKUP = 'PICKUP';
    const PROPERTY_CODE_ASK_RECIPIENT_DELIVERY_TIME = 'ASK_RECIPIENT_DELIVERY_TIME';
    const PROPERTY_CODE_NO_CONTACT_DELIVERY = 'NO_CONTACT_DELIVERY';
    const PROPERTY_CODE_DATE_DELIVERY = 'DATE_DELIVERY';
    const PROPERTY_CODE_TIME_INTERVAL = 'TIME_INTERVAL';
    const PROPERTY_CODE_DELIVERY_VARIANTS = 'DELIVERY_VARIANTS';
    const PROPERTY_CODE_EXACT_TIME = 'EXACT_TIME';
    const PROPERTY_CODE_COORDINATES = 'COORDINATES';

    /** @var  Basket */
    private $basket;
    /** @var  Order */
    private $order;
    /** @var  array */
    private $fields;
    /** @var  array */
    private $gifts;
    /** @var  array */
    private $sessionStorage;

    private $isValid = false;
    private $paySystems;
    private $deliveries;
    protected $userId;
    protected $profileId;
    protected $profile;

    /** @var Money */
    private $money;
    /**
     * @var string
     */
    private $selectedDeliveryId;
    private $selectedDelivery;
    private $deliveryVariants;
    /**
     * @var bool
     */
    private $logoutAfterCheckout = false;
    /**
     * @var mixed
     */
    private $selectedPaySystemId;
    private array $pickupList;
    /*private string $selectedPickupId;*/
    private array $lastOrder = [];

    public function __construct($component)
    {
        parent::__construct($component);

        Loader::includeModule("sale");
        Loader::includeModule("iblock");
    }

    public function onPrepareComponentParams($params) {
        return $params;
    }

    public function executeComponent() {
        $this->setFrameMode(false);
        $this->initServices();
        $this->initSessionStorage();
        $this->initBasket();
        $this->initProfile();
        $this->initLastOrder();
        $this->initOrder();
        $this->initFields();
        $this->initGifts();
        $this->calculateDeliveries();
        $this->setDeliveryList();
        $this->setPickupList();
        $this->setSelectedDelivery();
        $this->setPaySystemList();

        if ($this->request->isPost() && check_bitrix_sessid()) {
            $this->checkSkipVariants();
            $this->checkSkipDeliveryFields();
            $this->setProperties();
            $this->setPersonType();
            $this->setPaySystem();

            if ($this->isCreateOrder()) {
                $this->checkUser();
                $this->validateFields();
                $isRes = $this->saveOrder();

                if ($isRes) {
                    $jsonRes = array ("SUCCESS_URL" => $this->arResult["successUrl"]);
                    global $APPLICATION;
                    $APPLICATION->RestartBuffer();
                    echo json_encode($jsonRes);
                    die();
                }
            }
        }

        $this->setResultData();
        $this->includeComponentTemplate();
    }

    private function initServices() {
        $this->money = Module::getInstance()->getService('money');
    }

    private function setDeliveryList() {

        $this->selectedDeliveryId = $this->getFieldValue('DELIVERY_ID');

        if ($this->fields[self::PROPERTY_CODE_ASK_RECIPIENT_ADDRESS]['VALUE'] == 'Y') {
            $this->selectedDeliveryId = self::DELIVERY_ID_MOSCOW;
        }

        if ($this->selectedDeliveryId) {
            $_SESSION['ORDER_FIELDS']['DELIVERY_ID'] = $this->selectedDeliveryId;
        }

        if (!$this->selectedDeliveryId) {
            $this->selectedDeliveryId = $this->getDefaultDelivery();
        }

        $deliveryItem = null;
        if ($this->selectedDeliveryId) {
            $deliveryItem = \Bitrix\Sale\Delivery\Services\Manager::getObjectById($this->selectedDeliveryId);
        }

        $shipment = $this->order
            ->getShipmentCollection()
            ->createItem($deliveryItem);

        /** custom */

        $selectedVariant = $this->fields[Checkout::PROPERTY_CODE_DELIVERY_VARIANTS]['VALUE'];
        if ($selectedVariant && ($this->selectedDeliveryId != self::DELIVERY_ID_PICKUP) && (count($this->deliveryVariants) > 0)) {
            $info = $this->deliveryVariants[$selectedVariant];

            $service = \Bitrix\Sale\Delivery\Services\Manager::getById($this->selectedDeliveryId);
            $deliveryData = [
                'DELIVERY_ID' => $service['ID'],
                'DELIVERY_NAME' => $service['NAME'],
                'ALLOW_DELIVERY' => 'Y',
                'PRICE_DELIVERY' => $info['cost'],
                'CUSTOM_PRICE_DELIVERY' => 'Y'
            ];
            $shipment->setFields($deliveryData);
        }

        /** custom */

        $shipmentItemCollection = $shipment->getShipmentItemCollection();
        $shipment->setField('CURRENCY', $this->order->getCurrency());

        /** @var BasketItem $item */
        foreach ($this->order->getBasket() as $item) {
            /** @var ShipmentItem $shipmentItem */
            $shipmentItem = $shipmentItemCollection->createItem($item);
            $shipmentItem->setQuantity($item->getQuantity());
        }
        $this->deliveries = $this->getDeliveries($shipment);

        return $this;
    }

    private function getDefaultDelivery() {
        $mapSession = $this->getMapSession();
        if (in_array('DELIVERY_ID', $mapSession)) {
            $value = $this->sessionStorage['DELIVERY_ID'];
            unset($mapSession);
        }
        if (!empty($value)) {
            return $value;
        }

        $mapOrder = $this->getMapOrder();
        if (in_array('DELIVERY_ID', $mapOrder)) {
            $value = $this->lastOrder['DELIVERY_ID'];
            unset($mapOrder);
        }
        if (!empty($value)) {
            return $value;
        }

        return false;
    }

    private function setPickupList() {
        $this->pickupList = $this->getPickups();
        return $this;
    }

    private function getPickups() {
        $elements = [];
        $arSelect = ["ID", "NAME", "IBLOCK_ID", "PREVIEW_TEXT", "PROPERTY_pickupCoordinates"];
        $arFilter = ['IBLOCK_ID' => PICKUPS_IBLOCK_ID, 'ACTIVE' => 'Y'];
        $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
        while ($arRes = $res->GetNext()) {
            $element = [];
            if (!empty($arRes['PROPERTY_PICKUPCOORDINATES_VALUE'])) {
                $element['id'] = $arRes['ID'];
                $element['name'] = $arRes['NAME'];
                $element['description'] = $arRes['PREVIEW_TEXT'];
                $element['coords'] = $arRes['PROPERTY_PICKUPCOORDINATES_VALUE'];
            }
            $elements[$element['id']] = $element;
        }

        $pickupId = $this->fields[self::PROPERTY_CODE_PICKUP_ID]['VALUE'];
        if (!empty($pickupId)) {
            $elements[$pickupId]['checked'] = true;
        } else {
            reset($elements);
            $first = current($elements);
            $elements[$first['id']]['checked'] = true;
            $this->fields[self::PROPERTY_CODE_PICKUP_ID]['VALUE'] = $first['id'];
        }

        return $elements;
    }

    /**
     * @param Shipment $shipment
     *
     * @return array
     */
    private function getDeliveries(Shipment $shipment) {
        $deliveries = Manager::getRestrictedObjectsList($shipment);
        $arDeliveries = [];
        foreach ($deliveries as $delivery) {
            $deliveryId = $delivery->getId();

            $arDelivery = [
                'id' => $deliveryId,
                'name' => $delivery->getName(),
                'description' => $delivery->getDescription(),
                'currency' => $this->order->getCurrency(),
                'sort' => $delivery->getSort(),
                'code' => $delivery->getCode(),
                'price' => round($delivery->calculate($shipment)->getPrice(), 2),
            ];

            if ($deliveryId == self::DELIVERY_ID_MOSCOW) {
                $arDelivery['alias'] = 'moscow';
            }
            if ($deliveryId == self::DELIVERY_ID_OUT_MKAD) {
                $arDelivery['alias'] = 'outsideMKAD';
            }
            if ($deliveryId == self::DELIVERY_ID_PICKUP) {
                $arDelivery['alias'] = 'pickup';
            }

            if ($this->selectedDeliveryId && ($this->selectedDeliveryId == $deliveryId)) {
                $arDelivery['checked'] = true;
            }
            $arDeliveries[] = $arDelivery;
        }
        if (!$this->selectedDeliveryId) {
            $arDeliveries[0]['checked'] = true;
        }

        return $arDeliveries;
    }

    private function calculateDeliveries() {
        $step = $this->request->get('step');
        $action = $this->request->get('action');
        if (($step > 3) || ($action == self::ACTION_CREATE_ORDER)) {
            $this->setVariants();
        }
    }

    private function checkSkipVariants() {
        $variants = $this->fields[Checkout::PROPERTY_CODE_DELIVERY_VARIANTS];
        if ($this->fields[Checkout::PROPERTY_CODE_ASK_RECIPIENT_DELIVERY_TIME]['VALUE'] != 'Y') {
            return;
        }
        unset($this->fields[Checkout::PROPERTY_CODE_DELIVERY_VARIANTS]['OPTIONS']['delivery_exact']);

        if ($variants['VALUE'] != 'delivery_exact') {
            return;
        }
        $this->fields[Checkout::PROPERTY_CODE_DELIVERY_VARIANTS]['VALUE'] = 'delivery_standart';
    }

    private function checkSkipDeliveryFields() {

        if ($this->fields[Checkout::PROPERTY_CODE_ASK_RECIPIENT_DELIVERY_TIME]['VALUE'] == 'Y') {
            $this->fields[Checkout::PROPERTY_CODE_TIME_INTERVAL]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_EXACT_TIME]['VALUE'] = '';
        }

        if ($this->fields[Checkout::PROPERTY_CODE_ASK_RECIPIENT_ADDRESS]['VALUE'] == 'Y') {
            $this->fields[Checkout::PROPERTY_CODE_CITY]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_STREET]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_HOUSE]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_BUILDING]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_FLAT]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_PRIVATE_HOUSE]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_PICKUP]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_PICKUP_ID]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_COORDINATES]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_ASK_RECIPIENT_DELIVERY_TIME]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_NO_CONTACT_DELIVERY]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_DATE_DELIVERY]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_TIME_INTERVAL]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_DELIVERY_VARIANTS]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_EXACT_TIME]['VALUE'] = '';
        }

        $deliveryId = $this->getFieldValue('DELIVERY_ID');
        if ($deliveryId == self::DELIVERY_ID_PICKUP) {
            $this->fields[Checkout::PROPERTY_CODE_CITY]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_STREET]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_HOUSE]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_BUILDING]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_FLAT]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_PRIVATE_HOUSE]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_ASK_RECIPIENT_DELIVERY_TIME]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_NO_CONTACT_DELIVERY]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_DATE_DELIVERY]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_TIME_INTERVAL]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_DELIVERY_VARIANTS]['VALUE'] = '';
            $this->fields[Checkout::PROPERTY_CODE_EXACT_TIME]['VALUE'] = '';
        }
    }

    private function setVariants() {
        $fixOrderPrice = 2500;
        $this->deliveryVariants = [];

        if ($this->fields[self::PROPERTY_CODE_ASK_RECIPIENT_ADDRESS]['VALUE'] == 'Y') {
            return;
        }

        $deliveryId = $this->getFieldValue('DELIVERY_ID');
        if ($deliveryId == self::DELIVERY_ID_PICKUP) {
            return;
        }

        $basketPrice = $this->order->getPrice();
        $variants = $this->getVariantsHints();

        if ($deliveryId == self::DELIVERY_ID_MOSCOW) {

            $basePrice = 300;
            if ($basketPrice > $fixOrderPrice) {
                $basePrice = 0;
            }

            foreach ($variants as $code => $info) {
                $this->deliveryVariants[$code] = $info;
                if ($code == 'delivery_airport') {
                    $this->deliveryVariants[$code]['cost'] = $info['cost'];
                } else {
                    $this->deliveryVariants[$code]['cost'] = $basePrice + $info['cost'];
                }
            }
            $this->deliveryVariants['delivery_standart']['toFree'] = $fixOrderPrice - $basketPrice;
        }

        if ($deliveryId == self::DELIVERY_ID_OUT_MKAD) {

            $distance = $this->getFieldValue('distance');
            if (empty($distance)) {
                return;
            }
            $distance = intval($distance / 1000);
            $basePrice = 300;
            if ($basketPrice <= $fixOrderPrice) {
                $calcBasePrice = $basePrice + ($distance * 40);
            } else {
                $calcBasePrice = ($distance - (($basketPrice - $fixOrderPrice) / 100)) * 40;
                if ($calcBasePrice && !($calcBasePrice + abs($calcBasePrice))) {
                    $calcBasePrice = 0;
                }
            }

            foreach ($variants as $code => $info) {
                $this->deliveryVariants[$code] = $info;
                if ($code == 'delivery_airport') {
                    $this->deliveryVariants[$code]['cost'] = $info['cost'];
                } else {
                    $this->deliveryVariants[$code]['cost'] = $calcBasePrice + $info['cost'];
                }
            }
            $this->deliveryVariants['delivery_standart']['toFree'] = ($distance - (($basketPrice - $fixOrderPrice) / 100)) * 100;
        }

        if (empty($this->fields[self::PROPERTY_CODE_DELIVERY_VARIANTS]['VALUE'])) {
            $this->fields[self::PROPERTY_CODE_DELIVERY_VARIANTS]['VALUE'] = 'delivery_standart';
        }
    }

    /**
     * @param Order $order
     *
     * @return Order
     */
    protected function getOrderClone(Order $order)
    {
        /** @var Order $orderClone */
        $orderClone = $order->createClone();

        $clonedShipment = $this->getCurrentShipment($orderClone);
        if (!empty($clonedShipment))
        {
            $clonedShipment->setField('CUSTOM_PRICE_DELIVERY', 'N');
        }

        return $orderClone;
    }

    public function getCurrentShipment(Order $order) {
        /** @var Shipment $shipment */
        foreach ($order->getShipmentCollection() as $shipment) {
            if (!$shipment->isSystem())
                return $shipment;
        }

        return null;
    }

    private function initBasket() {
        $this->basket = Basket::loadItemsForFUser(\CSaleBasket::GetBasketUserID(), $this->getSiteId());
        $count = 0;
        /** @var BasketItem $item */
        foreach ($this->basket as $item) {
            $item->canBuy() && $count++;
        }

        if ($count > 0) {
            return;
        }
        LocalRedirect($this->arParams['PATH_TO_BASKET']);
    }

    private function setResultData() {

        $this->arResult['basketItems'] = $this->getBasketItems();
        $this->arResult = array_merge(
            $this->arResult, array(
            "fields" => $this->getResultFields(),
            "gifts" => $this->getResultGifts(),
            "customerFields" => $this->getCustomerFields(),
            "recipientFields" => $this->getRecipientFields(),
            "settingsFields" => $this->getSettingsFields(),
            "deliveryFields" => $this->getDeliveryFields(),
            "deliveryVariants" => $this->deliveryVariants,
            "exactTimeItems" => $this->getExactTimeItems(),
            "totalInfo" => $this->getTotalInfo(),
            "deliveries" => $this->deliveries,
            "pickups" => $this->pickupList,
            "selectedDelivery" => $this->selectedDelivery,
            "pay_systems" => $this->paySystems,
            "selectedPaySystemId" => $this->selectedPaySystemId,
        )
        );

        $this->arResult['expandSteps'] = $this->getExpandSteps();

        if (($this->fields[self::PROPERTY_CODE_ASK_RECIPIENT_ADDRESS]['VALUE'] == 'Y') && $this->arResult['expandSteps']['stepBox_3']) {
            $this->arResult['expandSteps']['stepBox_3'] = false;
            $this->arResult['expandSteps']['stepBox_4'] = false;
            $this->arResult['expandSteps']['stepBox_5'] = true;
        }

        if (($this->selectedDeliveryId == self::DELIVERY_ID_PICKUP) && $this->arResult['expandSteps']['stepBox_4']) {

            $this->arResult['expandSteps']['stepBox_4'] = false;
            $this->arResult['expandSteps']['stepBox_5'] = true;
        }
    }

    private function getBasketItems() {
        $basketItems = [];
        $productsIds = [];
        $result = $this->basket->refreshData(array('PRICE', 'QUANTITY', 'COUPONS'));
        /** @var \Bitrix\Sale\BasketItem $basketItem */
        foreach ($this->basket as $basketItem) {
            if (!$basketItem->canBuy()) {
                continue;
            }

            $productId = $basketItem->getProductId();
            $productsIds[] = $productId;

            $item = array(
                'id' => $basketItem->getId(),
                'productId' => $productId,
                'customPrice' => $basketItem->isCustomPrice(),
                'price' => $basketItem->getPrice(),
                'totalPrice' => $basketItem->getFinalPrice(),
                'basePrice' => ($basketItem->isCustomPrice()) ? $basketItem->getPrice() : $basketItem->getBasePrice(),
                'discountPrice' => $basketItem->getDiscountPrice(),
                'fullPrice' => $basketItem->getBasePrice() * $basketItem->getQuantity(),
                'quantity' => $basketItem->getQuantity(),
                'name' => $basketItem->getField("NAME"),
                'detailPageUrl' => $basketItem->getField("DETAIL_PAGE_URL"),
                'measureName' => $basketItem->getField("MEASURE_NAME"),
                'props' => $basketItem->getPropertyCollection()->getPropertyValues()
            );

            $basketItems[] = $item;
        }

        if (count($productsIds) > 0) {
            $products = $this->getProducts($productsIds);
            foreach ($basketItems as &$item) {
                $item['picture'] = $products[$item['productId']]['PREVIEW_PICTURE']['src'];
            }
        }

        return $basketItems;
    }

    private function initOrder() {
        global $USER;
        $userId = $USER->GetID() ? : \CSaleUser::GetAnonymousUserID();
        $this->order = Order::create($this->getSiteId(), $userId);
        $this->order->getDiscount()->calculate();
        $this->order->setPersonTypeId(self::ID_PERSON_TYPE_INDIVIDUAL);
        $this->order->setBasket($this->basket);
    }

    private function initFields() {
        $mapSession = $this->getMapSession();
        $this->fields = [];
        $propertyCollection = $this->order->getPropertyCollection();
        /** @var PropertyValue $property */
        foreach ($propertyCollection as $property) {
            $propertyFields = $property->getProperty();
            $propertyFields["FIELD_NAME"] = $this->createFieldName($property->getPropertyId());

            if (!$this->request->isPost()) {
                if ($value = $this->getDefaultValue($propertyFields["CODE"])) {
                    $propertyFields["VALUE"] = $value;
                }

                if (($propertyFields["CODE"] == self::PROPERTY_CODE_DELIVERY_VARIANTS) && !$value) {
                    $propertyFields["VALUE"] = 'delivery_standart';
                }
            }

            if ($this->request->isPost()) {
                $value = trim($this->getFieldValue($propertyFields["FIELD_NAME"]));
                if (in_array($propertyFields["CODE"], $mapSession)) {
                    $_SESSION['ORDER_FIELDS'][$propertyFields["CODE"]] = $value;
                }
                $propertyFields["VALUE"] = $value;
            }

            $this->fields[$property->getField('CODE')] = $propertyFields;
        }

        unset($mapSession);

    }

    private function createFieldName($id) {
        return "ORDER_PROP_" . $id;
    }

    private function getFieldValue($name) {
        return trim($this->request->getPost($name));
    }

    private function validateFields() {
        $fields = array();
        foreach ($this->fields as $field) {
            if ($field["REQUIRED"] != "Y") {
                continue;
            }

            $fields[$field["CODE"]] = $field["VALUE"];
        }

        $verifier = new Verifier($fields);
        foreach ($fields as $fieldName => $fieldValue) {
            $item = $verifier->field($fieldName)->notEmpty()->ifValid();

            /** При необходимости более детальной валидации, раскомментировать и доработать регулярки */

            $field = $this->fields[$fieldName];
            if ($field["IS_EMAIL"] == "Y") {
                $item->email("");
            }

            /*if ($field["IS_PHONE"] == "Y") {
                    $item->byRegularExpression("/(^(?!\+.*\(.*\).*\-\-.*$)(?!\+.*\(.*\).*\-$)(\+[0-9]{1} \([0-9]{3}\) [0-9]{3} [0-9]{4})$)|(^[0-9]{1,2}$)/");
                }*/

            $this->fields[$fieldName]["error"] = $item->hasErrors();
        }
        $this->isValid = $verifier->isValid();
    }

    private function saveOrder() {
        if (!$this->isValid) {
            return false;
        }

        $userId = $this->user()->GetID();
        if ($userId !== $this->order->getUserId()) {
            $this->order->setFieldNoDemand('USER_ID', $userId);
        }

        $result = $this->order->save();
        if ($result->isSuccess()) {
            $this->saveProfile();
            $this->arResult["orderId"] = $result->getId();
            $this->arResult["successUrl"] = str_replace("#ORDER_ID#", $result->getId(), $this->arParams["SUCCESS_URL"]);
            $this->logoutAfterCheckout && $this->user()->Logout();
            return true;
        } else {
            $this->arResult["errors"] = $result->getErrorMessages();
            return false;
        }
    }

    /**
     * @return $this
     */
    private function setProperties() {
        $skipDeliveryFields = $this->getSkipDeliveryFields();
        $skipPickupFields = $this->getSkipPickupFields();

        /** @var PropertyValue $propertyValue */
        foreach ($this->order->getPropertyCollection() as $propertyValue) {
            $value = $this->fields[$propertyValue->getField('CODE')]["VALUE"];
            if ($propertyValue->getField('CODE') == self::PROPERTY_CODE_DATE_DELIVERY) {
                $date = $this->fields[$propertyValue->getField('CODE')]["VALUE"];
                $value = date("d.m.Y", strtotime($date));
            }
            if ($propertyValue->getField('CODE') == self::PROPERTY_CODE_PICKUP) {
                $pickupId = $this->fields[self::PROPERTY_CODE_PICKUP_ID]['VALUE'];
                if (!empty($pickupId) && !empty($this->pickupList[$pickupId])) {
                    $value = $this->pickupList[$pickupId]['name'];
                }
            }
            if (($this->selectedDelivery['alias'] == 'pickup') && (in_array($propertyValue->getField('CODE'), $skipDeliveryFields))) {
                $value = '';
            }
            if (($this->selectedDelivery['alias'] != 'pickup') && (in_array($propertyValue->getField('CODE'), $skipPickupFields))) {
                $value = '';
            }
            $propertyValue->setValue($value);
        }

        return $this;
    }

    private function getSkipDeliveryFields() {
        return [
            self::PROPERTY_CODE_STREET,
            self::PROPERTY_CODE_HOUSE,
            self::PROPERTY_CODE_FLAT,
            self::PROPERTY_CODE_CITY
        ];
    }

    private function getSkipPickupFields() {
        return [
            self::PROPERTY_CODE_PICKUP_ID,
            self::PROPERTY_CODE_PICKUP,
        ];
    }

    /**
     * @return $this
     */
    private function setPersonType() {
        $this->order->setPersonTypeId(self::ID_PERSON_TYPE_INDIVIDUAL);
        return $this;
    }

    protected function initProfile() {
        global $USER;
        if ($USER->IsAuthorized()) {
            if ($USER->getId()) {
                $this->userId = $USER->getId();
            }
        }

        if (!$this->userId) {
            return;
        }

        $dbUserProfiles = \CSaleOrderUserProps::GetList(
            ["ID" => "ASC"],
            [
                "PERSON_TYPE_ID" => self::ID_PERSON_TYPE_INDIVIDUAL,
                "USER_ID" => $this->userId
            ]
        );

        if ($arUserProfiles = $dbUserProfiles->fetch()) {
            $this->profileId = $arUserProfiles;
        }

        $this->profile = false;
        $profileData = [];

        $res = \CSaleOrderUserPropsValue::GetList(
            [], ["USER_PROPS_ID" => $this->profileId["ID"]], false, false, []
        );

        while ($profileItem = $res->fetch()) {
            $profileData[$profileItem['PROP_CODE']] = $profileItem['VALUE'];
        }
        $this->profile = $profileData;
    }

    private function initLastOrder() {
        if (!$this->userId) {
            return;
        }

        $lastOrderId = $this->getLastOrderId();
        if (!$lastOrderId) {
            return;
        }

        $fields = [];
        $mapOrder = $this->getMapOrder();

        /** @var Order $order */
        $order = Order::load($lastOrderId);

        $fields['DELIVERY_ID'] = $order->getField('DELIVERY_ID');

        /** @var PropertyValue $propertyValue */
        foreach ($order->getPropertyCollection() as $propertyValue) {
            if (in_array($propertyValue->getField('CODE'), $mapOrder)) {
                $fields[$propertyValue->getField('CODE')] = $propertyValue->getValue();
            }
        }
        $this->lastOrder = $fields;
    }

    private function getLastOrderId() {
        $orderData = Order::getList([
            'select' => ['ID'],
            'filter' => ['=USER_ID' => $this->userId],
            'order' => ['ID' => 'DESC'],
            'limit' => 1
        ]);
        if ($order = $orderData->fetch()) {
            return $order['ID'];
        }
        return false;
    }

    private function setPaySystemList() {
        $payment = $this->order
            ->getPaymentCollection()
            ->getInnerPayment();
        if (!$payment)
        {
            $payment = $this->order->getPaymentCollection()->createInnerPayment();
        }
        $this->paySystems = $this->getPaySystems($payment);

        return $this;
    }

    /**
     * @param Payment $payment
     *
     * @return array
     */
    private function getPaySystems(Payment $payment) {

        $this->selectedPaySystemId = $this->getFieldValue('PAY_SYSTEM_ID');

        if (!$this->selectedPaySystemId) {
            $this->selectedPaySystemId = $this->getDefaultPaySystem();
        }

        $paySystems = PaySystem\Manager::getListWithRestrictions($payment);
        $arPaySystems = array();
        foreach($paySystems as $paySystem) {
            $arPaySystems[$paySystem['ID']] = $paySystem;
            if ($this->selectedPaySystemId == $paySystem['ID']) {
                $arPaySystems[$paySystem['ID']]['CHECKED'] = 'Y';
            }
        }

        if (!$this->selectedPaySystemId) {
            reset($arPaySystems);
            $arPaySystem = current($arPaySystems);
            $arPaySystems[$arPaySystem['ID']]["CHECKED"] = "Y";
        }

        return $arPaySystems;
    }

    private function getDefaultPaySystem() {
        $mapSession = $this->getMapSession();
        if (in_array('PAY_SYSTEM_ID', $mapSession)) {
            $value = $this->sessionStorage['PAY_SYSTEM_ID'];
            unset($mapSession);
        }
        if (!empty($value)) {
            return $value;
        }
        return false;
    }

    private function setPaySystem() {
        $payment = $this->order
            ->getPaymentCollection()
            ->getInnerPayment();
        $paySystemId = $this->request->getPost("PAY_SYSTEM_ID");
        if (!$paySystemId) {
            return $this;
        }
        $paySystem = $this->paySystems[$paySystemId];
        $payment->setFields(array(
            'PAY_SYSTEM_ID' => $paySystem['ID'],
            'PAY_SYSTEM_NAME' => $paySystem['NAME'],
            'SUM' => $this->order->getPrice()
        ));
        $this->paySystems[$paySystemId]['CHECKED'] = 'Y';
        $this->selectedPaySystemId = $paySystemId;
        return $this;
    }

    private function saveProfile() {
        $profileFieldCode = "FIO";

        $profileId = false;
        $dbUserProfiles = \CSaleOrderUserProps::GetList(
            ["ID" => "ASC"],
            [
                "PERSON_TYPE_ID" => self::ID_PERSON_TYPE_INDIVIDUAL,
                "USER_ID" => $this->order->getUserId()
            ]
        );

        if ($arUserProfiles = $dbUserProfiles->fetch())
            $profileId = $arUserProfiles["ID"];

        \CSaleOrderUserProps::DoSaveUserProfile(
            $this->order->getUserId(),
            $profileId,
            $this->getProfileName($profileFieldCode),
            $this->order->getPersonTypeId(),
            $this->order->getId(),
            $errors
        );
    }

    private function getProfileName($code) {
        if (!$code)
            return false;

        foreach ($this->fields as $field) {
            if ($field["CODE"] == $code)
                return $field["VALUE"];
        }
    }

    private function getProducts($productsIds) {
        $arProducts = [];
        if (count($productsIds) > 0) {
            $arSelect = ["ID", "NAME", "IBLOCK_ID", "DETAIL_PAGE_URL", "PREVIEW_PICTURE"];
            $arFilter = ["ID" => $productsIds, 'IBLOCK_ID' => CATALOG_IBLOCK_ID];
            $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
            while ($arRes = $res->GetNext()) {
                if (!empty($arRes['PREVIEW_PICTURE'])) {
                    $arRes['PREVIEW_PICTURE'] = CFile::ResizeImageGet($arRes['PREVIEW_PICTURE'], ["width" => self::IMAGE_SIZE_WIDTH, "height" => self::IMAGE_SIZE_HEIGHT], BX_RESIZE_IMAGE_EXACT);
                } else {
                    $arRes['PREVIEW_PICTURE']['src'] = '/static/img/@2x/pa-order-item.jpg';
                }
                $arProducts[$arRes['ID']] = $arRes;
            }
        }
        return $arProducts;
    }

    private function getCustomerFields() {
        return [
            self::PROPERTY_CODE_NAME,
            self::PROPERTY_CODE_LAST_NAME,
            self::PROPERTY_CODE_PHONE,
            self::PROPERTY_CODE_EMAIL,
        ];
    }

    private function getRecipientFields() {
        return [
            self::PROPERTY_CODE_RECIPIENT_NAME,
            self::PROPERTY_CODE_RECIPIENT_LAST_NAME,
            self::PROPERTY_CODE_RECIPIENT_PHONE,
        ];
    }

    private function getSettingsFields() {
        return [
            self::PROPERTY_CODE_HAND_OVER_INCOGNITO,
            self::PROPERTY_CODE_PHOTO_WITH_RECIPIENT,
            self::PROPERTY_CODE_SURPRISE,
            self::PROPERTY_CODE_NOT_CALL_ADVANCE,
            self::PROPERTY_CODE_NOT_KNOW_RECIPIENT_PHONE,
            self::PROPERTY_CODE_ASK_RECIPIENT_ADDRESS,
        ];
    }

    private function getDeliveryFields() {
        return [
            self::PROPERTY_CODE_CITY,
            self::PROPERTY_CODE_STREET,
            self::PROPERTY_CODE_HOUSE,
            self::PROPERTY_CODE_BUILDING,
            self::PROPERTY_CODE_FLAT,
            self::PROPERTY_CODE_PRIVATE_HOUSE,
        ];
    }

    private function getTotalInfo() {
        return [
            'basketPrice' => $this->getBasketPrice(),
            'deliveryPrice' => $this->order->getDeliveryPrice(),
            'basketPriceFormatted' => $this->money->format($this->getBasketPrice()),
            'discount' => $this->money->format($this->getDiscount()),
            'totalPrice' => $this->money->format($this->getTotalPrice())
        ];
    }

    private function getBasketPrice() {
        $basketPrice = 0;
        foreach ($this->arResult['basketItems'] as $item) {
            $basketPrice = $basketPrice + $item['fullPrice'];
        }
        return $basketPrice;
    }

    private function getDiscount() {
        return (int) $this->getBasketPrice() - (int) $this->order->getBasket()->getPrice();
    }

    private function getTotalPrice() {
        return $this->order->getPrice();
    }

    private function setSelectedDelivery() {

        if (!$this->selectedDeliveryId) {
            return;
        }
        foreach ($this->deliveries as $delivery) {
            if ($this->selectedDeliveryId != $delivery['id']) {
                continue;
            }
            $this->selectedDelivery = $delivery;
        }
    }

    private function isCreateOrder() {
        return $this->request->getPost("action") == self::ACTION_CREATE_ORDER;
    }

    private function checkUser() {
        $userEmail = $this->fields[self::PROPERTY_CODE_EMAIL]['VALUE'];
        if (!$userEmail) {
            return;
        }
        if ($this->user()->IsAuthorized()) {
            return;
        }
        if (!$this->authorizeUser($userEmail)) {
            $this->fields[self::PROPERTY_CODE_EMAIL]['error_register'] = Loc::getMessage('USER_ALREADY_EXISTS_MESS');
            $this->fields[self::PROPERTY_CODE_EMAIL]['error'] = true;
        } else {
            $this->logoutAfterCheckout = true;
        }
    }

    private function authorizeUser($userEmail) {
        $user = $this
            ->user()
            ->GetList($by, $order, ['=EMAIL' => $userEmail], ['ID'])
            ->Fetch();

        if ($user) {
            $this->user()->Authorize($user['ID']);
            return true;
        }

        $password = md5($this->fields[self::PROPERTY_CODE_EMAIL]['VALUE'] . $this->fields[self::PROPERTY_CODE_LAST_NAME]['VALUE']);
        $userId = $this->user()->Add(array(
            'ACTIVE' => 'Y',
            'LOGIN' => $this->fields[self::PROPERTY_CODE_EMAIL]['VALUE'],
            'PASSWORD' => $password,
            'CONFIRM_PASSWORD' => $password,
            'EMAIL' => $this->fields[self::PROPERTY_CODE_EMAIL]['VALUE'],
            'NAME' => $this->fields[self::PROPERTY_CODE_NAME]['VALUE'],
            'LAST_NAME' => $this->fields[self::PROPERTY_CODE_LAST_NAME]['VALUE'],
            'PERSONAL_PHONE' => $this->fields[self::PROPERTY_CODE_PHONE]['VALUE'],
        ));

        return $this->user()->Authorize($userId);
    }

    /**
     * @return CUser
     */
    private function user() {
        global $USER;

        return $USER;
    }

    private function getResultFields() {
        return $this->fields;
    }

    private function getResultGifts() {
        return $this->gifts;
    }

    private function initSessionStorage() {
        if (isset($_SESSION['ORDER_FIELDS'])) {
            $this->sessionStorage = $_SESSION['ORDER_FIELDS'];
        }
    }

    private function initGifts() {
        $arGifts = $this->getGifts();
        $basketProducts = [];
        /** @var \Bitrix\Sale\BasketItem $basketItem */
        foreach ($this->basket as $basketItem) {
            $basketProducts[$basketItem->getProductId()]['productId'] = $basketItem->getProductId();
            $basketProducts[$basketItem->getProductId()]['basketId'] = $basketItem->getId();
        }

        foreach ($arGifts as & $gift) {
            if (is_array($basketProducts[$gift['id']])) {
                $gift['inBasket'] = true;
                $gift['basketId'] = $basketProducts[$gift['id']]['basketId'];
            }
            $gift['picture'] = CFile::ResizeImageGet($gift['picture'], ["width" => self::IMAGE_GIFT_SIZE_WIDTH, "height" => self::IMAGE_GIFT_SIZE_HEIGHT], BX_RESIZE_IMAGE_EXACT);
        }
        $this->gifts = $arGifts;
    }

    private function getGifts() {
        $elements = [];
        $arSelect = [
            'ID', 'IBLOCK_ID', 'NAME', 'PREVIEW_PICTURE', 'PRICE_1'
        ];
        $arFilter = [
            'IBLOCK_ID' => CATALOG_IBLOCK_ID,
            'SECTION_CODE' => 'gifts',
            'ACTIVE' => 'Y',
            '=AVAILABLE' => 'Y'
        ];

        $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
        while ($arRes = $res->Fetch()) {
            $item['id'] = $arRes['ID'];
            $item['name'] = $arRes['NAME'];
            $item['picture'] = $arRes['PREVIEW_PICTURE'];
            $item['price'] = intval($arRes['PRICE_1']);
            $elements[$arRes['ID']] = $item;
        }
        return $elements;
    }

    private function getExpandSteps() {
        if (!$this->request->isAjaxRequest()) {
            $expand['stepBox_1'] = true;
            return $expand;
        }

        $requestStep = $this->request->get('step');
        $expand['stepBox_1'] = true;
        if (!empty($requestStep)) {
            $start = 2;
            while ($start <= (int)$requestStep) {
                $stepBox = 'stepBox_' . $start;
                $expand[$stepBox] = $this->isExpand($stepBox);
                $start++;
            }
            return $expand;
        }
        return $expand;
    }

    private function isExpand($stepBox) {
        $requiredFields = [];
        $isExpand = true;
        switch ($stepBox) {
            case 'stepBox_2':
                $requiredFields = $this->getRequiredCustomerFields();
                break;
            case 'stepBox_3':
                $requiredFields = $this->getRequiredRecipientFields();
                break;
            case 'stepBox_4':
                $requiredFields = $this->getRequiredDeliveryFields();
                break;
            case 'stepBox_5':
                $requiredFields = $this->getRequiredDateDeliveryFields();
                break;
        }

        foreach ($requiredFields as $field) {
            if (empty(trim($this->arResult['fields'][$field]['VALUE']))) {
                $isExpand = false;
            }
        }

        return $isExpand;
    }

    private function getRequiredCustomerFields() {
        return [
            self::PROPERTY_CODE_NAME,
            self::PROPERTY_CODE_LAST_NAME,
            self::PROPERTY_CODE_PHONE,
            self::PROPERTY_CODE_EMAIL,
        ];
    }

    private function getRequiredRecipientFields() {
        $fields = [
            self::PROPERTY_CODE_RECIPIENT_NAME,
            self::PROPERTY_CODE_RECIPIENT_LAST_NAME,
        ];
        if ($this->fields[self::PROPERTY_CODE_NOT_KNOW_RECIPIENT_PHONE]['VALUE'] != 'Y') {
            $fields[] = self::PROPERTY_CODE_RECIPIENT_PHONE;
        }
        return $fields;
    }

    private function getRequiredDeliveryFields() {
        if ($this->selectedDelivery['alias'] == 'pickup') {
            return [];
        }

        $fields = [
            self::PROPERTY_CODE_STREET,
            self::PROPERTY_CODE_HOUSE,
        ];

        if ($this->fields[self::PROPERTY_CODE_PRIVATE_HOUSE]['VALUE'] != 'Y') {
            $fields[] = self::PROPERTY_CODE_FLAT;
        }

        if ($this->selectedDelivery['alias'] == 'outsideMKAD') {
            $fields[] = self::PROPERTY_CODE_CITY;
        }
        return $fields;
    }

    private function getRequiredDateDeliveryFields() {

        $fields = [
            self::PROPERTY_CODE_DATE_DELIVERY,
        ];
        if (($this->fields[self::PROPERTY_CODE_ASK_RECIPIENT_DELIVERY_TIME]['VALUE'] != 'Y') && ($this->fields[Checkout::PROPERTY_CODE_DELIVERY_VARIANTS]['VALUE'] == 'delivery_standart')) {
            $fields[] = self::PROPERTY_CODE_TIME_INTERVAL;
        }
        return $fields;
    }

    private function getVariantsHints() {

        return [
            'delivery_standart' => [
                'cost' => 0,
            ],
            'delivery_night' => [
                'hint' => \WS_PSettings::getFieldValue('delivery_night'),
                'cost' => 800,
                'desc' => 'Доставка 22:00 до 6:00',
            ],
            'delivery_express' => [
                'hint' => \WS_PSettings::getFieldValue('delivery_express'),
                'cost' => 600,
                'desc' => 'в течении 2-х часов',
            ],
            'delivery_exact' => [
                'hint' => \WS_PSettings::getFieldValue('delivery_exact'),
                'cost' => 1000,
            ],
            'delivery_airport' => [
                'hint' => \WS_PSettings::getFieldValue('delivery_airport'),
                'cost' => 1500,
                'desc' => 'Внуково, Домодедово, Шереметьево',
            ],
        ];
    }

    private function getDefaultValue($fieldName) {
        $mapSession = $this->getMapSession();
        if (in_array($fieldName, $mapSession)) {
            $value = $this->sessionStorage[$fieldName];
            unset($mapSession);
        }
        if (!empty($value)) {
            return $value;
        }

        $mapProfile = $this->getMapProfile();
        if (in_array($fieldName, $mapProfile)) {
            $value = $this->profile[$fieldName];
            unset($mapProfile);
        }
        if (!empty($value)) {
            return $value;
        }

        $mapOrder = $this->getMapOrder();
        if (in_array($fieldName, $mapOrder)) {
            $value = $this->lastOrder[$fieldName];
            unset($mapOrder);
        }
        if (!empty($value)) {
            return $value;
        }

        return false;
    }

    private function getMapSession() {
        return [
            self::PROPERTY_CODE_NAME, self::PROPERTY_CODE_LAST_NAME, self::PROPERTY_CODE_PHONE, self::PROPERTY_CODE_EMAIL,
            self::PROPERTY_CODE_NOT_CALL_ME, self::PROPERTY_CODE_RECEIVE_PERSONALLY, self::PROPERTY_CODE_RECIPIENT_NAME,
            self::PROPERTY_CODE_RECIPIENT_LAST_NAME, self::PROPERTY_CODE_RECIPIENT_PHONE, self::PROPERTY_CODE_ADD_FREE_POSTCARD,
            self::PROPERTY_CODE_TEXT_FREE_POSTCARD, self::PROPERTY_CODE_ADD_DESIGN_POSTCARD, self::PROPERTY_CODE_TEXT_DESIGN_POSTCARD,
            self::PROPERTY_CODE_HAND_OVER_INCOGNITO, self::PROPERTY_CODE_PHOTO_WITH_RECIPIENT, self::PROPERTY_CODE_SURPRISE,
            self::PROPERTY_CODE_NOT_CALL_ADVANCE, self::PROPERTY_CODE_NOT_KNOW_RECIPIENT_PHONE, self::PROPERTY_CODE_ASK_RECIPIENT_ADDRESS,
            self::PROPERTY_CODE_CITY, self::PROPERTY_CODE_STREET, self::PROPERTY_CODE_HOUSE, self::PROPERTY_CODE_BUILDING,
            self::PROPERTY_CODE_FLAT, self::PROPERTY_CODE_PRIVATE_HOUSE, self::PROPERTY_CODE_PICKUP_ID,
            self::PROPERTY_CODE_ASK_RECIPIENT_DELIVERY_TIME, self::PROPERTY_CODE_NO_CONTACT_DELIVERY, self::PROPERTY_CODE_DATE_DELIVERY,
            self::PROPERTY_CODE_TIME_INTERVAL, self::PROPERTY_CODE_DELIVERY_VARIANTS, self::PROPERTY_CODE_COORDINATES, 'DELIVERY_ID', 'PAY_SYSTEM_ID',

        ];
    }

    private function getMapProfile() {
        return [
            self::PROPERTY_CODE_NAME, self::PROPERTY_CODE_LAST_NAME, self::PROPERTY_CODE_PHONE, self::PROPERTY_CODE_EMAIL,
            self::PROPERTY_CODE_RECIPIENT_NAME, self::PROPERTY_CODE_RECIPIENT_LAST_NAME, self::PROPERTY_CODE_RECIPIENT_PHONE,
        ];
    }

    private function getMapOrder() {
        return [
            self::PROPERTY_CODE_NAME, self::PROPERTY_CODE_LAST_NAME, self::PROPERTY_CODE_PHONE, self::PROPERTY_CODE_EMAIL,
            self::PROPERTY_CODE_NOT_CALL_ME, self::PROPERTY_CODE_RECEIVE_PERSONALLY, self::PROPERTY_CODE_RECIPIENT_NAME,
            self::PROPERTY_CODE_RECIPIENT_LAST_NAME, self::PROPERTY_CODE_RECIPIENT_PHONE, self::PROPERTY_CODE_CITY,
            self::PROPERTY_CODE_STREET, self::PROPERTY_CODE_HOUSE, self::PROPERTY_CODE_BUILDING,
            self::PROPERTY_CODE_FLAT, self::PROPERTY_CODE_PRIVATE_HOUSE, self::PROPERTY_CODE_PICKUP_ID,
            self::PROPERTY_CODE_ASK_RECIPIENT_DELIVERY_TIME, self::PROPERTY_CODE_NO_CONTACT_DELIVERY,
            self::PROPERTY_CODE_COORDINATES, 'DELIVERY_ID',
        ];
    }

    private function getExactTimeItems() {
        $res['hours'] = $this->getHours();
        $res['min'] = $this->getMinutes();
        return $res;
    }

    private function getHours() {
        return [
            '00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14',
            '15', '16', '17', '18', '19', '20', '21', '22', '23',
        ];
    }

    private function getMinutes() {
        return [
            '00', '05', '10', '15', '20', '25', '30', '35', '40', '45', '50', '55',
        ];
    }
}