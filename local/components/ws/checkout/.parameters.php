<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arComponentParameters = array(
    "PARAMETERS" => array(
        "PATH_TO_BASKET" => array(
            "NAME" => GetMessage("CHECKOUT_PATH_TO_BASKET"),
            "TYPE" => "STRING",
            "MULTIPLE" => "N",
            "DEFAULT" => "basket.php",
            "COLS" => 25,
            "PARENT" => "ADDITIONAL_SETTINGS",
        ),
        "SET_TITLE" => array(),
    )
);