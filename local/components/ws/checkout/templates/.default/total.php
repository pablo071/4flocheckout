<?
use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */
/** @var array $arHeaders */
?>
<?php
$totalInfo = $arResult['totalInfo'];
?>
<div class="sCheckout__title">
    <div class="h3"><?=Loc::getMessage("CH_TOTAL_TITLE");?></div>
    <div class="sCheckout__totalPrice">
        <div data-total-price="15000" class="h5"><?=$totalInfo['totalPrice'];?> р.</div>
    </div>
</div>