<?
use WS\Components\Checkout;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */
/** @var array $arHeaders */
?>
<?php
$fio = $arResult['fields'][Checkout::PROPERTY_CODE_FIO];
?>
<input id="fioField" type="hidden" name="<?=$fio['FIELD_NAME'];?>" value="<?=$fio['VALUE'];?>">
<input type="hidden" data-id="delivery-price">
<input
    id="pickupIdField"
    type="hidden"
    name="<?=$arResult['fields'][Checkout::PROPERTY_CODE_PICKUP_ID]['FIELD_NAME'];?>"
    value="<?=$arResult['fields'][Checkout::PROPERTY_CODE_PICKUP_ID]['VALUE'];?>"
>
<input
    id="exactTimeField"
    type="hidden"
    name="<?=$arResult['fields'][Checkout::PROPERTY_CODE_EXACT_TIME]['FIELD_NAME'];?>"
    value="<?=$arResult['fields'][Checkout::PROPERTY_CODE_EXACT_TIME]['VALUE'];?>"
>