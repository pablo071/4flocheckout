<?php

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */
/** @var array $arHeaders */
?>

<?php
$gifts = $arResult['gifts'];
?>

<div class="sCatalog sCatalog--checkout-new" id="sCatalog">
    <div class="sCatalog__sliderCheckut sCatalog__sliderCheckut--new-js swiper-container">
        <div class="swiper-wrapper">
            <?foreach ($gifts as $gift):?>
            <div class="sCatalog__slide swiper-slide">
                <label for="giftCard-<?=$gift['id'];?>" class="custom-input">
                    <input
                        class="custom-input__input gift_cards order_basket_action"
                        type="checkbox"
                        id="giftCard-<?=$gift['id'];?>"
                        name="gift_<?=$gift['id'];?>"
                        <?if ($gift['inBasket']):?>
                            checked
                            data-id="<?=$gift['basketId'];?>"
                            data-action="removeBasketItem"
                        <?else:?>
                            data-action="addToBasket"
                        <?endif;?>
                        data-step="2"
                        data-productId="<?=$gift['id'];?>"
                    />
                    <span class="custom-input__lab"></span>
                </label>
                <div class="prod-item">
                    <div class="prod-item__img-wrap">
                        <img class="res-i" src="<?=$gift['picture']['src'];?>" alt=""/>
                    </div>
                    <div class="h6"><?=$gift['name'];?></div>
                    <div class="h5"><?=Loc::getMessage("CH_GIFT_PRICE_TEXT", Array ("#PRICE#" => intval($gift['price'])));?></div>
                </div>
            </div>
            <?endforeach;?>
        </div>
    </div>
    <div class="swiper-button-hand swiper-button-hand-prev swiper-button-prev"></div>
    <div class="swiper-button-hand swiper-button-hand-next swiper-button-next"></div>
</div>

<script>
    var defaultSl = (_defaultSl = {
        slidesPerView: 1,
        watchOverflow: true,
        spaceBetween: 0
    }, _defineProperty(_defaultSl, "watchOverflow", true), _defineProperty(_defaultSl, "loop", true), _defineProperty(_defaultSl, "lazy", {
        loadPrevNext: true,
        loadPrevNextAmount: 7 // loadOnTransitionStart: true

    }), _defaultSl);
    var swiper11 = new Swiper('.sCatalog__sliderCheckut--new-js', _objectSpread(_objectSpread({}, defaultSl), {}, {
        spaceBetween: 15,
        slidesPerView: 2,
        navigation: {
            nextEl: '.sCatalog--checkout-new .swiper-button-next',
            prevEl: '.sCatalog--checkout-new .swiper-button-prev'
        },
        breakpoints: {
            // when window width is >= 320px
            // when window width is >= 480px
            576: {
                slidesPerView: 4
            },
            // when window width is >= 640px
            // 992: {
            // 	slidesPerView: 4,
            // },
            1200: {
                slidesPerView: 5,
                spaceBetween: 35
            }
        }
    }));
</script>