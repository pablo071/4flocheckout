<?php
use WS\Components\Checkout;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */
/** @var array $arHeaders */
?>
<?php
$fields = $arResult['fields'];
$customerFields = $arResult['customerFields'];
$checkbox[] = $arResult['fields'][Checkout::PROPERTY_CODE_NOT_CALL_ME];
$checkbox[] = $arResult['fields'][Checkout::PROPERTY_CODE_RECEIVE_PERSONALLY];
?>

<div class="row">
    <?foreach ($fields as $code => $field) {
        if (!in_array($code, $customerFields)) continue;

        $typeField = 'text';
        if ($field['CODE'] == Checkout::PROPERTY_CODE_PHONE) {
            $typeField = 'tel';
        }
        if ($field['CODE'] == Checkout::PROPERTY_CODE_EMAIL) {
            $typeField = 'email';
        }
        ?>
        <div class="col-md-6">
            <div class="form-wrap__input-wrap form-group">
                <label for="ID_<?=$field['CODE'];?>">
                    <span class="form-wrap__title"><?=$field['NAME'];?></span>
                    <input
                            class="form-wrap__input form-control"
                            type="<?=$typeField;?>"
                            placeholder=""
                            name="<?=$field['FIELD_NAME'];?>"
                            <?if($field['REQUIRED'] == 'Y'):?>
                                required="required"
                            <?endif;?>
                            <?if(!empty($field['MAXLENGTH'])):?>
                                maxlength="<?=$field['MAXLENGTH'];?>"
                            <?endif;?>
                            id="ID_<?=$field['CODE'];?>"
                            value="<?=$field['VALUE'];?>"
                    />
                </label>
            </div>
        </div>
    <? } ?>
</div>
<div class="form-wrap__custom-input-wrap">
    <?foreach ($checkbox as $item):?>
        <label class="custom-input" for="ID_<?=$item['CODE'];?>">
            <input
                    id="ID_<?=$item['CODE'];?>"
                    class="custom-input__input"
                    type="checkbox"
                    name="<?=$item['FIELD_NAME'];?>"
                    <?if($item['VALUE'] == 'Y'):?>
                        checked
                    <?endif;?>
                    value="Y"
            />
            <span class="custom-input__lab"></span>
            <span class="custom-input__text"><?=$item['NAME'];?></span>
        </label>
    <?endforeach;?>
</div>