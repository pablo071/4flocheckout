<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */
/** @var array $arHeaders */
?>
<?php
$paySystems = $arResult['pay_systems'];
?>
<?foreach ($paySystems as $paySystem):?>
    <label class="custom-radio">
        <input
                type="radio"
                name="PAY_SYSTEM_ID"
                id="ID_PAY_SYSTEM_ID_<?=$paySystem["ID"];?>"
                value="<?=$paySystem["ID"];?>"
                <?if($paySystem['CHECKED'] == 'Y'):?>
                    checked
                <?endif;?>
        >
        <span class="custom-radio__text">
            <span class="custom-radio__textWrap">
                <span class="custom-radio__radioTitle"><?=$paySystem["NAME"];?></span>
            </span>
        </span>
    </label>
<?endforeach;?>