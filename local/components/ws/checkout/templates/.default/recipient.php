<?php

use Bitrix\Main\Localization\Loc;
use WS\Components\Checkout;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */
/** @var array $arHeaders */
?>
<?php
$fields = $arResult['fields'];
$recipientFields = $arResult['recipientFields'];
$addFreePostCard = $arResult['fields'][Checkout::PROPERTY_CODE_ADD_FREE_POSTCARD];
$textFreePostCard = $arResult['fields'][Checkout::PROPERTY_CODE_TEXT_FREE_POSTCARD];
$addDesignPostCard = $arResult['fields'][Checkout::PROPERTY_CODE_ADD_DESIGN_POSTCARD];
$textDesignPostCard = $arResult['fields'][Checkout::PROPERTY_CODE_TEXT_DESIGN_POSTCARD];
?>

<div class="row">
    <?foreach ($fields as $code => $field) {
        if (!in_array($code, $recipientFields)) continue;

        $isRequired = true;
        $typeField = 'text';
        if ($field['CODE'] == Checkout::PROPERTY_CODE_RECIPIENT_PHONE) {
            $typeField = 'tel';
            if ($fields[Checkout::PROPERTY_CODE_NOT_KNOW_RECIPIENT_PHONE]['VALUE'] == 'Y') {
                $isRequired = false;
            }
        }
        ?>
        <div class="col-md-6">
            <div class="form-wrap__input-wrap form-group">
                <label for="ID_<?=$field['CODE'];?>">
                    <span class="form-wrap__title"><?=$field['NAME'];?></span>
                    <input
                            class="form-wrap__input form-control"
                            type="<?=$typeField;?>"
                            placeholder=""
                            name="<?=$field['FIELD_NAME'];?>"
                            <?if($isRequired):?>
                                required="required"
                            <?endif;?>
                            <?if(!empty($field['MAXLENGTH'])):?>
                                maxlength="<?=$field['MAXLENGTH'];?>"
                            <?endif;?>
                            id="ID_<?=$field['CODE'];?>"
                            value="<?=$field['VALUE'];?>"
                    />
                </label>
            </div>
        </div>
    <? } ?>
</div>

<label class="custom-input-radio">
    <span class="custom-input-radio__topLine">
        <span class="row justify-content-between">
            <span class="col-sm-auto d-flex">
                <input
                        id="ID_<?=$addFreePostCard['CODE'];?>"
                        class="custom-input-radio__hidden"
                        type="checkbox"
                        name="<?=$addFreePostCard['FIELD_NAME'];?>"
                        <?if($addFreePostCard['VALUE'] == 'Y'):?>
                            checked
                        <?endif;?>
                        value="Y"
                />
                <span class="custom-input-radio__lab"></span>
                <span class="custom-input-radio__text"><?=$addFreePostCard['NAME'];?></span>
            </span>
            <?if (!empty($textFreePostCard['MAXLENGTH'])):?>
                <span class="col-sm-auto">
                    <span class="custom-input-radio__characters"><?=$textFreePostCard['MAXLENGTH'];?> <?=Loc::getMessage("CH_SYMBOLS_TEXT");?></span>
                </span>
            <?endif;?>
        </span>
    </span>
</label>

<div class="form-wrap__input-wrap form-group">
    <input
            id="ID_<?=$textFreePostCard['CODE'];?>"
            class="form-wrap__input form-control"
            type="text"
            placeholder=""
            name="<?=$textFreePostCard['FIELD_NAME'];?>"
            <?if(!empty($textFreePostCard['MAXLENGTH'])):?>
                maxlength="<?=$textFreePostCard['MAXLENGTH'];?>"
            <?endif;?>
            value="<?=$textFreePostCard['VALUE'];?>"
            <?if($addFreePostCard['VALUE'] != 'Y'):?>
                disabled
            <?else:?>
                required="required"
            <?endif;?>
    />
</div>

<label class="custom-input-radio">
    <span class="custom-input-radio__topLine">
        <span class="row justify-content-between">
            <span class="col-sm-auto d-flex">
                <input
                        id="ID_<?=$addDesignPostCard['CODE'];?>"
                        class="custom-input-radio__hidden"
                        type="checkbox"
                        name="<?=$addDesignPostCard['FIELD_NAME'];?>"
                        <?if($addDesignPostCard['VALUE'] == 'Y'):?>
                            checked
                        <?endif;?>
                        value="Y"
                />
                <span class="custom-input-radio__lab"></span>
                <span class="custom-input-radio__text"><?=$addDesignPostCard['NAME'];?></span>
            </span>
            <?
            $maxLength = 900;
            if (!empty($textDesignPostCard['MAXLENGTH'])) {
                $maxLength = $textDesignPostCard['MAXLENGTH'];
            }?>
            <span class="col-sm-auto">
                <span id="maxLengthError" class="custom-input-radio__characters"><?=$maxLength;?> <?=Loc::getMessage("CH_SYMBOLS_TEXT");?></span>
            </span>
        </span>
    </span>
</label>

<div class="form-wrap__input-wrap form-group">
    <textarea
            id="ID_<?=$textDesignPostCard['CODE'];?>"
            class="form-wrap__input form-control"
            placeholder=""
            name="<?=$textDesignPostCard['FIELD_NAME'];?>"
            <?if(!empty($textDesignPostCard['MAXLENGTH'])):?>
                maxlength="<?=$textDesignPostCard['MAXLENGTH'];?>"
            <?endif;?>
            <?if($addDesignPostCard['VALUE'] != 'Y'):?>
                disabled
            <?else:?>
                required="required"
            <?endif;?>
    ><?=$textDesignPostCard['VALUE'];?></textarea>
</div>