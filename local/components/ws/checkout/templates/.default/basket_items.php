<? use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */
/** @var array $arHeaders */
?>
<?php
$basketItems = $arResult['basketItems'];
?>

<div class="sCheckout__totalOrder">
    <?foreach ($basketItems as $item):?>
        <?if ($item['props']['giftDesignPostcard']['VALUE'] == 'Y') {
            continue;
        }
        ?>
        <div class="sCheckout__orderItem">
            <div class="row">
                <div class="col-sm-8 d-flex">
                    <a class="sCheckout__imgWrap" target="_blank" href="<?=$item['detailPageUrl'];?>">
                        <img class="res-i" src="<?=$item['picture'];?>" alt=""/>
                    </a>
                    <a class="sCheckout__name" target="_blank" href="<?=$item['detailPageUrl'];?>">
                        <?=$item['name'];?> <span> <?=intval($item['quantity']);?> <?=$item['measureName'];?></span>
                    </a>
                </div>
                <div class="col">
                    <div class="sCheckout__itemPrice">
                        <div class="h5"><?=$item['totalPrice'];?> р.</div>
                        <button class="sCheckout__remove btn link-pink order_basket_action" data-action="removeBasketItem" data-id="<?=$item['id'];?>" type="button">
                            <?=Loc::getMessage("CH_REMOVE_ITEM");?>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    <?endforeach;?>
</div>