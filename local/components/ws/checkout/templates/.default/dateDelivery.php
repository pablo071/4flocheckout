<?php

use Bitrix\Main\Localization\Loc;
use WS\Components\Checkout;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */
/** @var array $arHeaders */
?>
<?php
$askRecipient = $arResult['fields'][Checkout::PROPERTY_CODE_ASK_RECIPIENT_DELIVERY_TIME];
$noContact = $arResult['fields'][Checkout::PROPERTY_CODE_NO_CONTACT_DELIVERY];
$date = $arResult['fields'][Checkout::PROPERTY_CODE_DATE_DELIVERY];
$interval = $arResult['fields'][Checkout::PROPERTY_CODE_TIME_INTERVAL];
$variants = $arResult['fields'][Checkout::PROPERTY_CODE_DELIVERY_VARIANTS];
$exactTime = explode(':', $arResult['fields'][Checkout::PROPERTY_CODE_EXACT_TIME]['VALUE']);
$deliveryVariants = $arResult['deliveryVariants'];
$expandSteps = $arResult['expandSteps'];
$hideInterval = '';
if(($askRecipient['VALUE'] == 'Y') || ($variants['VALUE'] != 'delivery_standart')) {
    $hideInterval = 'style="display:none"';
}
if ($variants['VALUE'] != 'delivery_exact') {
    $exactTime = [];
}
$freeSum = 2500;
$sum = 0;
$basketPrice = $arResult['totalInfo']['basketPrice'];
if ($basketPrice < $freeSum) {
    $sum = $freeSum - $basketPrice;
}
$action = false;
?>
<div class="form-wrap__custom-input-wrap row">
    <div class="col-sm-6">
        <label for="ID_<?=$askRecipient['CODE'];?>" class="custom-input">
            <input
                id="ID_<?=$askRecipient['CODE'];?>"
                class="custom-input__input order_action"
                type="checkbox"
                name="<?=$askRecipient['FIELD_NAME'];?>"
                <?if($askRecipient['VALUE'] == 'Y'):?>
                    checked
                <?endif;?>
                value="Y"
                data-action="nextStep"
                data-step="4"
            />
            <span class="custom-input__lab"></span>
            <span class="custom-input__text"><?=$askRecipient['NAME'];?></span>
        </label>
    </div>
    <div class="col-sm-6">
        <label for="ID_<?=$noContact['CODE'];?>" class="custom-input">
            <input
                id="ID_<?=$noContact['CODE'];?>"
                class="custom-input__input"
                type="checkbox"
                name="<?=$noContact['FIELD_NAME'];?>"
                <?if($noContact['VALUE'] == 'Y'):?>
                    checked
                <?endif;?>
                value="Y"
            />
            <span class="custom-input__lab"></span>
            <span class="custom-input__text"><?=$noContact['NAME'];?></span>
        </label>
    </div>
    <div class="col-md-6">
        <div class="form-wrap__smText">
            <?=Loc::getMessage("CH_DATE_TITLE");?>
        </div>
        <div class="form-wrap__input-wrap form-group">
            <input
                    class="form-wrap__input form-control"
                    type="date"
                    required="required"
                    name="<?=$date['FIELD_NAME'];?>"
                    value="<?=$date['VALUE'];?>"
                    min="<?=date('Y-m-d', time());?>"
            />
        </div>
        <!-- +e.input-wrap-->
    </div>
    <div class="col-md-6">
        <div id="intervalTitle" class="form-wrap__smText" <?=$hideInterval;?>>
            <?=Loc::getMessage("CH_TIME_TITLE");?>
        </div>
        <div id="timeIntervalBox" class="catalog-sort" <?=$hideInterval;?>>
            <select
                    class="catalog-sort__input form-control time-interval"
                    id="time-interval"
                    name="<?=$interval['FIELD_NAME'];?>"
                    <?if(($askRecipient['VALUE'] != 'Y') && ($variants['VALUE'] == 'delivery_standart')):?>
                        required="required"
                    <?endif;?>
            >
                <option id="chooseInterval" <?if(empty($interval['VALUE'])):?>selected<?endif;?> value="">Выберите промежуток</option>
                <?foreach ($interval['OPTIONS'] as $code => $value):?>
                    <option <?if($code == $interval['VALUE']):?>selected<?endif;?> value="<?=$code;?>"><?=$value;?></option>
                <?endforeach;?>
            </select>
        </div>
    </div>
    <div class="col-md-6 order-md-last">
        <div id="intervalDescBox" <?=$hideInterval;?>>
            <p><?=Loc::getMessage("CH_DATE_DESC");?></p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-wrap__deliveryType">
            <?foreach ($variants['OPTIONS'] as $type => $name):?>
                <?
                if ($expandSteps['stepBox_5']) {
                    if ($variants['VALUE'] != $type) {
                        $action = true;
                    }
                }
                ?>

                <label class="custom-radio">
                    <input
                            id="v-<?=$type;?>"
                            class="js-delivery-type-change<?if($action):?> order_action<?endif;?>"
                            type="radio"
                            data-base-price="<?if(($type != 'delivery_standart')) {echo $deliveryVariants[$type]['cost'];}?>"
                            name="<?=$variants['FIELD_NAME'];?>"
                            value="<?=$type;?>"
                            <?if($variants['VALUE'] == $type):?>
                                checked
                            <?endif;?>
                            <?if($action):?>
                                data-action="nextStep"
                                data-step="4"
                            <?endif;?>
                    />
                    <span class="custom-radio__text">
                        <span class="custom-radio__textWrap">
                            <span class="custom-radio__radioTitle">
                                <?=$name;?>
                                <?if(!empty($deliveryVariants[$type]['hint'])):?>
                                    <span class="custom-radio__infoBlock">?
                                        <span class="custom-radio__infoHidden">
                                            <?=$deliveryVariants[$type]['hint'];?>
                                        </span>
                                    </span>
                                <?endif;?>
                                <?if($type == 'delivery_exact'):?>
                                    <span class="custom-radio__selectBlock">
                                        <select
                                                class="custom-radio__input form-control<?if($action):?> order_action<?endif;?>"
                                                id="time-clock"
                                                <?if($action):?>
                                                    data-action="nextStep"
                                                    data-step="4"
                                                <?endif;?>
                                        >
                                            <?foreach ($arResult['exactTimeItems']['hours'] as $hour):?>
                                                <option
                                                    <?if('00' == $hour):?>
                                                        id="timeClockFirst"
                                                    <?endif;?>
                                                    <?if($exactTime[0] == $hour):?>
                                                        selected
                                                    <?endif;?>
                                                        value="<?=$hour?>"
                                                >
                                                    <?=$hour?>
                                                </option>
                                            <?endforeach;?>
                                        </select>
                                        <select
                                                class="custom-radio__input form-control<?if($action):?> order_action<?endif;?>"
                                                id="time-minutes"
                                                <?if($action):?>
                                                    data-action="nextStep"
                                                    data-step="4"
                                                <?endif;?>
                                        >
                                            <?foreach ($arResult['exactTimeItems']['min'] as $min):?>
                                                <option
                                                    <?if('00' == $min):?>
                                                        id="timeMinFirst"
                                                    <?endif;?>
                                                    <?if($exactTime[1] == $min):?>
                                                        selected
                                                    <?endif;?>
                                                        value="<?=$min?>"
                                                >
                                                    <?=$min?>
                                                </option>
                                            <?endforeach;?>
                                        </select>
                                    </span>
                                <?endif;?>
                            </span>
                            <?if(!empty($deliveryVariants[$type]['desc'])):?>
                                <span class="custom-radio__radioDescr">
                                    <?=$deliveryVariants[$type]['desc'];?>
                                </span>
                            <?endif;?>
                        </span>
                        <span class="custom-radio__deliveryPrice">
                            <?=$deliveryVariants[$type]['cost'] . 'р';?>
                        </span>
                    </span>
                </label>
                <?if(($type == 'delivery_standart') && ($sum > 0)):?>
                    <div class="form-wrap__freeShipping">
                        <?=Loc::getMessage("CH_FREE_DELIVERY_INFO", Array ("#SUM#" => $sum));?>
                    </div>
                <?endif;?>
            <?endforeach;?>
        </div>
    </div>
</div>