<?php

use WS\Components\Checkout;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */
/** @var array $arHeaders */
?>
<?php
$fields = $arResult['fields'];
$expandSteps = $arResult['expandSteps'];
$privateHouse = $fields[Checkout::PROPERTY_CODE_PRIVATE_HOUSE];
$deliveryFields = $arResult['deliveryFields'];
$deliveries = $arResult['deliveries'];
$pickups = $arResult['pickups'];
$coord = $fields[Checkout::PROPERTY_CODE_COORDINATES];
$selectedDelivery = $arResult['selectedDelivery'];
if ($selectedDelivery) {
    $checkedDelivery = $selectedDelivery['alias'];
} else {
    foreach ($deliveries as $delivery) {
        if ($delivery['checked']) {
            $checkedDelivery = $delivery['alias'];
            break;
        }
    }
}
$tabsClass = '';
if (($checkedDelivery == 'moscow') || ($checkedDelivery == 'outsideMKAD')) {
    $tabsClass .= ' active';
    if ($checkedDelivery == 'outsideMKAD') {
        $tabsClass .= ' tabs__show-all';
    }
}
$action = false;
?>
<div class="tabs">
    <div class="tabs__caption" data-tab="tabs-delivery">
        <?foreach ($deliveries as $delivery):?>

        <?
        if ($expandSteps['stepBox_4'] || $expandSteps['stepBox_5']) {
            if ($delivery['checked'] != 'Y') {
                $action = true;
            }
        }
        ?>

            <input
                    class="delivery_button_hidden<?if($action):?> order_action<?endif;?>"
                    id="ID_DELIVERY_ID_<?=$delivery['id'];?>"
                    type="radio"
                    name="DELIVERY_ID"
                    value="<?=$delivery['id'];?>"
                <?if($delivery['checked'] == 'Y'):?>
                    checked
                <?endif;?>
                <?if($action):?>
                    data-action="nextStep"
                    data-step="3"
                <?endif;?>
            >
        <label for="ID_DELIVERY_ID_<?=$delivery['id'];?>" class="delivery_button custom-radio">
            <div class="tabs__btn btn<?if($delivery['checked']):?> active<?endif;?>"
                 data-selected="<?if($delivery['checked']):?>true<?else:?>false<?endif;?>"
                 data-delivery-name="<?=$delivery['alias'];?>"
                 data-controls="<?if($delivery['alias']=='pickup'):?>pickup<?else:?>delivery<?endif;?>"
                <?if($delivery['alias']=='outsideMKAD'):?>
                    data-show="all"
                <?endif;?>
            >
                <?=$delivery['name'];?>
            </div>
        </label>
        <?endforeach;?>
    </div>
    <div class="tabs__wrap" data-content="tabs-delivery">
        <div class="tabs__content<?=$tabsClass;?>" data-id="delivery">
            <div class="row align-items-center">
                <?$actionCity = false;?>
                <?$actionStreet = false;?>
                <?foreach ($fields as $code => $field):?>
                    <?if (!in_array($code, $deliveryFields)) {
                        continue;
                    }?>
                    <?if ($code == Checkout::PROPERTY_CODE_PRIVATE_HOUSE) { continue; }?>
                    <?
                    $tabHide = '';
                    $required = true;
                    if ($code == Checkout::PROPERTY_CODE_CITY) {
                        $tabHide = ' tabs__hide';
                        if ((($checkedDelivery == 'moscow') || ($checkedDelivery == 'pickup'))) {
                            $field['VALUE'] = 'Москва';
                        }
                        if ($expandSteps['stepBox_4'] || $expandSteps['stepBox_5']) {
                            if ($checkedDelivery == 'outsideMKAD') {
                                $actionCity = true;
                            }
                        }
                    }
                    if ($code == Checkout::PROPERTY_CODE_STREET) {
                        if ($expandSteps['stepBox_4'] || $expandSteps['stepBox_5']) {
                            if ($checkedDelivery == 'moscow') {
                                $actionStreet = true;
                            }
                        }
                    }
                    if ($code == Checkout::PROPERTY_CODE_BUILDING) {
                        $required = false;
                    }
                    if (($privateHouse['VALUE'] == 'Y') && ($code == Checkout::PROPERTY_CODE_FLAT)) {
                        $required = false;
                        $field['VALUE'] = '';
                    }
                    ?>

                    <div class="col-md-6 col-xl-4<?=$tabHide;?>">
                        <div class="tabs__input-wrap form-group">
                            <label for="ID_<?=$field['CODE'];?>">
                                <span class="tabs__title"><?=$field['NAME'];?></span>
                                <input
                                        class="tabs__input form-control<?if($actionCity || $actionStreet):?> order_action<?endif;?>"
                                        name="<?=$field['FIELD_NAME'];?>"
                                        id="ID_<?=$field['CODE'];?>"
                                        value="<?=$field['VALUE'];?>"
                                        type="text"
                                        placeholder=""
                                        <?if($required):?>
                                            data-required="<?=strtolower($field['CODE']);?>"
                                        <?endif;?>
                                        <?if($actionCity || $actionStreet):?>
                                            data-action="nextStep"
                                            data-step="3"
                                        <?endif;?>
                                />
                            </label>
                        </div>
                        <!-- +e.input-wrap-->
                    </div>
                <?endforeach;?>
                <?if ($privateHouse):?>
                    <div class="tabs__homeCol col-sm align-self-end">
                        <label class="custom-input" for="ID_<?=$privateHouse['CODE'];?>">
                            <input
                                    class="custom-input__input"
                                    id="ID_<?=$privateHouse['CODE'];?>"
                                    type="checkbox"
                                    name="<?=$privateHouse['FIELD_NAME'];?>"
                                    <?if($privateHouse['VALUE'] == 'Y'):?>
                                        checked
                                    <?endif;?>
                                    value="Y"
                            />
                            <span class="custom-input__lab"></span>
                            <span class="custom-input__text"><?=$privateHouse['NAME'];?></span>
                        </label>
                    </div>
                <?endif;?>
                <!-- сюда js рендерит стоимость доставки -->
                <input type="hidden" name="calcCost" data-id="delivery-price" value="<?=$_REQUEST['calcCost'];?>">
                <!-- сюда js рендерит введеный пользователем конкатенированный адрес -->
                <input type="hidden" name="selectedAddress" data-id="selected-adress" value="<?=$_REQUEST['selectedAddress'];?>">
                <!-- сюда js рендерит расстояние в метрах -->
                <input type="hidden" name="distance" data-id="distance" value="<?=$_REQUEST['distance'];?>">
                <!-- сюда js рендерит координату -->
                <input type="hidden" name="<?=$coord['FIELD_NAME'];?>" data-id="coordinates" value="<?=$coord['VALUE'];?>">
            </div>
        </div>
        <?if (count($pickups) > 0):?>
            <div class="tabs__content<?if($checkedDelivery == 'pickup'):?> active<?endif;?>" data-id="pickup">
                <div class="tabsAddress row">
                    <div class="tabsAddress__caption col-lg-5" data-tab="tabs-pickup">
                        <?foreach ($pickups as $pickup):?>
                            <div
                                class="tabsAddress__btn btn<?if($pickup['checked']):?> active<?endif;?>"
                                data-selected="<?if($pickup['checked']):?>true<?else:?>false<?endif;?>"
                                data-controls="shop-<?=$pickup['id'];?>"
                                data-coordinates="<?=$pickup['coords'];?>"
                                data-id="<?=$pickup['id'];?>"
                            >
                                <?=$pickup['name'];?>
                            </div>
                        <?endforeach;?>
                    </div>
                    <div class="tabsAddress__wrap col-lg" data-content="tabs-pickup">
                        <?foreach ($pickups as $pickup):?>
                            <div class="tabsAddress__content<?if($pickup['checked']):?> active<?endif;?>" data-id="shop-<?=$pickup['id'];?>">
                                <?=$pickup['description'];?>
                            </div>
                        <?endforeach;?>
                    </div>
                </div>
            </div>
        <?endif;?>
    </div>
</div>