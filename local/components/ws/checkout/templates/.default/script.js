window.onload = function () {

    // Код маски телефона
    function setCursorPosition(pos, elem) {
        elem.focus();
        if (elem.setSelectionRange) elem.setSelectionRange(pos, pos);
        else if (elem.createTextRange) {
            var range = elem.createTextRange();
            range.collapse(true);
            range.moveEnd("character", pos);
            range.moveStart("character", pos);
            range.select()
        }
    }

    var len = 0;
    function mask(event) {
        var matrix = "+7 (___) ___ ____",
            i = 0,
            def = matrix.replace(/\D/g, ""),
            val = this.value.replace(/\D/g, "");
        if (def.length >= val.length) val = def;
        this.value = matrix.replace(/[_\d]/g, function(a) {
            return  i < val.length ? val.charAt(i++) :  a
        });
        i = this.value.indexOf("_");
        if(val.length < len) i = this.value.lastIndexOf(val.substr(-1))+1;
        if (i != -1) {
            i < 5 && (i = 3);
            this.value = this.value.slice(0,i);
        }
        if (event.type == "blur") {
            if (this.value.length < 5) this.value = ""
        } else setCursorPosition(this.value.length, this);
        len = val.length;
    };

    $(document).ready(function () {
            var input = $(".tel");
            input.each(function () {
                document.addEventListener('input',this, mask,false);
                document.addEventListener('focus',this, mask,false);
                document.addEventListener('blur',this, mask,false);
                document.addEventListener('load',this, mask,false);
                this.addEventListener("input", mask, false);
                this.addEventListener("focus", mask, false);
                this.addEventListener("blur", mask, false);
                this.addEventListener("load", mask, false);
            });
        }
    );

    $(document).on('change','.email', function () {
        validate(this, "email");
    });

    $(document).on('change','.tel', function () {
        validate(this, "tel");
    });

    $(document).on('change','.name', function () {
        validate(this, "name");
    });

    $(document).on('change','.lastname', function () {
        validate(this, "lastname");
    });

    $(document).on('change','.def', function () {
        validate(this, "def");
    });
    $(document).on('click','.busket-promoCode__poromoBtn', function (e) {
        //e.preventDefault();
        var couponName = $(".busket-promoCode__promoInput").val();
        var check = false;
        if(couponName != ""){
            check = true;
        }
        if(check){
            $.ajax({
                "url": $('#bx-soa-order-form').attr("action"),
                "data": {coupon_check:"Y", coupon:couponName,},
                "type": "POST",
                "success": function (response) {
                    $(document).find('.busket-promoCode .error').remove();
                    data=$.parseJSON(response);
                    if(data.error_coupon){
                        $(".busket-promoCode__title").append("<span class='error'>"+data.error_coupon+"</span>");
                    }else {
                        //$(".busket-promoCode").addClass("active-promo");
                        //$(".busket-promoCode__poromoBtn").addClass("disableBtn");
                        //$(".busket-promoCode__result").text(data.base_price + "Р");
                        //$(".busket-promoCode__sale-result").text(data.discount_price + "Р").css("display", "inline");
                        document.location.reload(true);
                    }
                    //console.log(data);
                    //console.log(response);
                }
            });
        }else{
            $(document).find('.busket-promoCode .error').remove();
            $(".busket-promoCode__title").append("<span class='error'>Введите промокод</span>");
        }

    });


    var prevForm;
    $(document).on("click",".busket-buy-wrapper__btn-organization", function () {
        var nextForm = this;
        if (prevForm != nextForm) {
            $(".busket-buy-wrapper__btn-organization").removeClass("active-organization");
            $(this).addClass("active-organization");
            $('.wrap-form').fadeOut().removeClass('active-form');
            $('.validate-wrap__spisok').fadeOut();
            $(".wrap-form[data-form=" + $(this).data('organization') + "]").fadeIn().addClass('active-form');
            $('#spisok-' + $(this).data('organization')).fadeIn();
            $('#organization').attr('value', $(this).data('organization'));
            prevForm = this;
            ajaxFormOrg();
        }
    });


    function ajaxFormOrg() {
            var $formOrder = $('#bx-soa-order-form'),
                typeSelector = $formOrder.find('input[name="PERSON_TYPE"]');

            console.log(typeSelector);

            var $container = $('.busket-buy-form');
            var $formData = $formOrder.serialize() + '&is_ajax=' + 'Y' + '&change_person=' + 'Y';

            $.ajax({
                "url": $formOrder.attr("action"),
                "data": $formData,
                "type": $formOrder.attr("method"),
                "success": function (response) {
                    console.log('response person type');
                    $container.html(response);
                    var input = $(document).find(".tel");
                    console.log('phone-------------')
                    console.log(input)
                    debugger
                    input.each(function () {
                        this.addEventListener("input", mask, false);
                        this.addEventListener("focus", mask, false);
                        this.addEventListener("blur", mask, false);
                        $(this).trigger('blur');
                    });
                }
            });
        }

    $('body').on("submit", "#bx-soa-order-form", function (e) {
        var checkInput = $(".active-form .busket-buy-form__input");

        checkInput.each(function () {
            validate(this, this.classList[1]);
            if ($(this).val().length == 0 || $(this).parent().hasClass('error')) {
                e.preventDefault();
                var input = $(this).offset().top - $('.header').height();
                $('html').animate({
                    scrollTop: input
                }, 800);
                return false;
            }
        })
        if($(".active-form").find('.error').length == 0) {
            e.preventDefault();
            var $self = $(this);
            var $container = $('.busket-buy-form');
            var $formData = $self.serialize() + '&is_ajax=' + 'Y' + '&is_submit=' + 'Y';

            $.ajax({
                "url": $self.attr("action"),
                "data": $formData,
                "type": $self.attr("method"),
                "success": function (response) {
                    try {
                        var result = JSON.parse(response);
                        console.log('result');
                        console.log(result);
                        if (result.ORDER_ID > 0 && result.SUCCESS_URL) {
                            window.location.href = result.SUCCESS_URL;
                            return;
                        }
                    } catch (e) {
                        console.log('response');
                        console.log(response);
                        $container.html(response);
                        return;
                    }
                }
            });
        }
    });

    function validate(input,type) {
        switch (type) {
            case 'email':
                var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z0-9-]{2,10}\.)?[a-z0-9-]{2,10}$/i;
                var textSucces = $(input).prev().text();
                var blockNoValid = $('#'+ $(input.parentNode).data("block"));

                if ($(input).val().length > 0 && pattern.test($(input).val())) {
                    input.parentNode.classList.add("success");
                    input.parentNode.classList.remove("error");
                    blockNoValid.children().each(function () {
                        if ($(this).text().toLowerCase() == textSucces.toLowerCase()) {
                            $(this).fadeOut();
                        }
                    });
                } else {
                    input.parentNode.classList.remove("success");
                    input.parentNode.classList.add("error");
                    blockNoValid.children().each(function () {
                        if ($(this).text().toLowerCase() == textSucces.toLowerCase()) {
                            $(this).fadeIn();
                        }
                    });
                }
                break;
            case 'email-home':
                var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,6}\.)?[a-z]{2,6}$/i;
                if ($(input).val().length > 0 && pattern.test($(input).val())) {
                    input.parentNode.classList.add("success");
                    input.parentNode.classList.remove("error");
                    console.log(input.parentNode);
                } else {
                    input.parentNode.classList.remove("success");
                    input.parentNode.classList.add("error");
                }
                break;
            case 'tel':
                var textSucces = $(input).prev().text();
                var blockNoValid = $('#'+ $(input.parentNode).data("block"));
                if($(input).val().length == '17') {
                    input.parentNode.classList.add("success");
                    input.parentNode.classList.remove("error");
                    blockNoValid.children().each(function () {
                        if ($(this).text().toLowerCase() == textSucces.toLowerCase()) {
                            $(this).fadeOut();
                        }
                    });
                } else {
                    input.parentNode.classList.remove("success");
                    input.parentNode.classList.add("error");
                    blockNoValid.children().each(function () {
                        if ($(this).text().toLowerCase() == textSucces.toLowerCase()) {
                            $(this).fadeIn();
                        }
                    });
                }
                break;
            case 'name':
                var textSucces = $(input).prev().text();
                var blockNoValid = $('#'+ $(input.parentNode).data("block"));
                if($(input).val().length > 0) {
                    input.parentNode.classList.add("success");
                    input.parentNode.classList.remove("error");
                    blockNoValid.children().each(function () {
                        if ($(this).text().toLowerCase() == textSucces.toLowerCase()) {
                            $(this).fadeOut();
                        }
                    });
                } else {
                    input.parentNode.classList.remove("success");
                    input.parentNode.classList.add("error");
                    blockNoValid.children().each(function () {
                        if ($(this).text().toLowerCase() == textSucces.toLowerCase()) {
                            $(this).fadeIn();
                        }
                    });
                }
                break;
            case 'lastname':
                var textSucces = $(input).prev().text();
                var blockNoValid = $('#'+ $(input.parentNode).data("block"));
                if($(input).val().length > 0) {
                    input.parentNode.classList.add("success");
                    input.parentNode.classList.remove("error");
                    blockNoValid.children().each(function () {
                        if ($(this).text().toLowerCase() == textSucces.toLowerCase()) {
                            $(this).fadeOut();
                        }
                    });
                } else {
                    input.parentNode.classList.remove("success");
                    input.parentNode.classList.add("error");
                    blockNoValid.children().each(function () {
                        if ($(this).text().toLowerCase() == textSucces.toLowerCase()) {
                            $(this).fadeIn();
                        }
                    });
                }
                break;
            default:
                var textSucces = $(input).prev().text();
                var blockNoValid = $('#'+ $(input.parentNode).data("block"));
                if($(input).val().length > 0) {
                    input.parentNode.classList.add("success");
                    input.parentNode.classList.remove("error");
                    blockNoValid.children().each(function () {
                        if ($(this).text().toLowerCase() == textSucces.toLowerCase()) {
                            $(this).fadeOut();
                        }
                    });
                } else {
                    input.parentNode.classList.remove("success");
                    input.parentNode.classList.add("error");
                    blockNoValid.children().each(function () {
                        if ($(this).text().toLowerCase() == textSucces.toLowerCase()) {
                            $(this).fadeIn();
                        }
                    });
                }
                break;
        }
    }
};


$(".password-cor").on("keyup", function() { // Выполняем скрипт при изменении содержимого 2-го поля

    var value_input1 = $(".password").val(); // Получаем содержимое 1-го поля
    var value_input2 = $(this).val(); // Получаем содержимое 2-го поля

    if(value_input1 != value_input2) { // Условие, если поля не совпадают

        $(".message-error").removeClass('error-message'); // Выводим сообщение
        $("#btn-order-submit").attr("disabled", "disabled"); // Запрещаем отправку формы

    } else { // Условие, если поля совпадают

        $("#btn-order-submit").removeAttr("disabled");  // Разрешаем отправку формы
        $(".message-error").addClass('error-message'); // Скрываем сообщение

    }

});

