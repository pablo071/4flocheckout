<?php
use Bitrix\Main\Localization\Loc;
use Bitrix\Main;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * Bitrix template vars
 * @var array $arResult
 * @var array $arParams
 * @var \CBitrixComponentTemplate $this
 * @var \WS\Components\Checkout $component
 * @var string $templateFolder
 * @var string $templateName
 * @var string $templateFile
 * @var string $componentPath
 */

/**
 * Bitrix global vars
 * @global \CMain $APPLICATION
 * @global \CUser $USER
 * @global \CDataBase $DB
 */
global $APPLICATION, $USER, $DB;
$documentRoot = Main\Application::getDocumentRoot();
$expandSteps = $arResult['expandSteps'];
//var_dump($expandSteps);
?>
<div class="order-make__preloader js-order-preloader">
    <div class="preloader js-preloader">
        <div class="preloader__in text-type2 text-type2--bold">Загрузка</div>
    </div>
</div>

<div class="col-lg-6 col-xl pb-4">
    <div class="sCheckout__waterData">
        <div class="form-wrap">
            <form id="order_form" class="order-make__form" name="order_form" method="POST">
                <?
                echo bitrix_sessid_post();
                ?>
                <input type="hidden" name="example-input-field"/>
                <input class="order" type="hidden" name="order" value="Заявка  с сайта"/>
                <input class="utm_source" type="hidden" name="utm_source"/>
                <input class="utm_term" type="hidden" name="utm_term"/>
                <input class="utm_medium" type="hidden" name="utm_medium"/>
                <input class="utm_campaign" type="hidden" name="utm_campaign"/>

                <div id="stepBox_1" class="form-wrap__form">
                    <div class="orderStep">
                        <div class="orderStep__numb<?if($expandSteps['stepBox_1']):?> active<?endif;?>">
                            <?=Loc::getMessage("CH_CUSTOMER_STEP");?>
                        </div>
                        <div class="orderStep__text">
                            <?=Loc::getMessage("CH_CUSTOMER_TITLE");?>
                        </div>
                        <div class="orderStep__iconWrap">
                            <img class="res-i" src="/static/img/svg/checkout-chevron-down.svg" alt=""/>
                        </div>
                    </div>
                    <div class="form-wrap__contentIner">

                        <?include $documentRoot.$templateFolder.'/customer.php';?>

                        <?if(!$expandSteps['stepBox_2']):?>
                            <div class="orderButton">
                                <a class="orderButton__next btn btn-accent order_action" data-action="nextStep" data-step="2" data-next-step="false" href="#">
                                    <?=Loc::getMessage("CH_BUTTON_NEXT");?>
                                </a>
                            </div>
                        <?endif;?>
                    </div>
                </div>

                <div id="stepBox_2" class="form-wrap__form" data-id-step="buyer">
                    <div class="orderStep<?if(!$expandSteps['stepBox_2']):?> collapse<?endif;?>">
                        <div class="orderStep__numb<?if($expandSteps['stepBox_2']):?> active<?endif;?>">
                            <?=Loc::getMessage("CH_RECIPIENT_STEP");?>
                        </div>
                        <div class="orderStep__text">
                            <?=Loc::getMessage("CH_RECIPIENT_TITLE");?>
                        </div>
                        <div class="orderStep__iconWrap">
                            <img class="res-i" src="/static/img/svg/checkout-chevron-down.svg" alt=""/>
                        </div>
                    </div>
                    <div class="form-wrap__contentIner<?if(!$expandSteps['stepBox_2']):?> collapse<?endif;?>">

                        <?include $documentRoot.$templateFolder.'/recipient.php';?>

                        <?include $documentRoot.$templateFolder.'/giftPostcards.php';?>

                        <?include $documentRoot.$templateFolder.'/settings.php';?>

                        <?if(!$expandSteps['stepBox_3'] && !$expandSteps['stepBox_5']):?>
                            <div class="orderButton">
                                <a class="orderButton__next btn btn-accent order_action" data-action="nextStep" data-next-step="false" data-step="3" href="#">
                                    <?=Loc::getMessage("CH_BUTTON_NEXT");?>
                                </a>
                            </div>
                        <?endif;?>
                    </div>
                </div>

                <?
                $count = 0;
                if (!$expandSteps['stepBox_3'] && $expandSteps['stepBox_5']) {
                    $step3style = 'style="display:none"';
                    $count = 1;
                }?>
                <div id="stepBox_3" class="form-wrap__form" <?=$step3style;?> data-id-step="delivery-method">
                    <div class="orderStep<?if(!$expandSteps['stepBox_3']):?> collapse<?endif;?>">
                        <div class="orderStep__numb<?if($expandSteps['stepBox_3']):?> active<?endif;?>">
                            <?=Loc::getMessage("CH_DELIVERY_STEP");?>
                        </div>
                        <div class="orderStep__text">
                            <?=Loc::getMessage("CH_DELIVERY_TITLE");?>
                        </div>
                        <div class="orderStep__iconWrap">
                            <img class="res-i" src="/static/img/svg/checkout-chevron-down.svg" alt=""/>
                        </div>
                    </div>
                    <div class="form-wrap__contentIner<?if(!$expandSteps['stepBox_3']):?> collapse<?endif;?>">
                        <?include $documentRoot.$templateFolder.'/delivery.php';?>
                    </div>
                    <?if($expandSteps['stepBox_3']):?>
                        <div id="ya-map" class="ya-map"></div>
                    <?endif;?>
                    <?if(!$expandSteps['stepBox_4'] && !$expandSteps['stepBox_5'] && $expandSteps['stepBox_3']):?>
                        <div class="orderButton">
                            <a class="orderButton__next btn btn-accent" data-action="delivery-interval" data-step="4" href="#">
                                <?=Loc::getMessage("CH_BUTTON_NEXT");?>
                            </a>
                        </div>
                    <?endif;?>
                </div>

                <?if (!$expandSteps['stepBox_4'] && $expandSteps['stepBox_5']) {
                    $step4style = 'style="display:none"';
                    $count = 2;
                    if ($expandSteps['stepBox_3']) {
                        $count = 1;
                    }
                }?>
                <div id="stepBox_4" class="form-wrap__form" <?=$step4style;?> data-id-step="delivery-interval">
                    <div class="orderStep<?if(!$expandSteps['stepBox_4']):?> collapse<?endif;?>">
                        <div class="orderStep__numb<?if($expandSteps['stepBox_4']):?> active<?endif;?>">
                            <?=Loc::getMessage("CH_DATE_DELIVERY_STEP");?>
                        </div>
                        <div class="orderStep__text">
                            <?=Loc::getMessage("CH_DATE_DELIVERY_TITLE");?>
                        </div>
                        <div class="orderStep__iconWrap"><img class="res-i" src="/static/img/svg/checkout-chevron-down.svg" alt=""/>
                        </div>
                    </div>
                    <div class="form-wrap__contentIner<?if(!$expandSteps['stepBox_4']):?> collapse<?endif;?>">

                        <?include $documentRoot.$templateFolder.'/dateDelivery.php';?>


                        <?if(!$expandSteps['stepBox_5']):?>
                            <div class="orderButton">
                                <div class="orderButton">
                                    <a class="orderButton__next btn btn-accent order_action" data-action="nextStep" data-step="5" href="#">
                                        <?=Loc::getMessage("CH_BUTTON_NEXT");?>
                                    </a>
                                </div>
                            </div>
                        <?endif;?>
                    </div>
                </div>

                <div id="stepBox_5" class="form-wrap__form">
                    <div class="orderStep<?if(!$expandSteps['stepBox_5']):?> collapse<?endif;?>">
                        <div class="orderStep__numb<?if($expandSteps['stepBox_5']):?> active<?endif;?>">
                            <?= 5 - $count;?>
                        </div>
                        <div class="orderStep__text">
                            <?=Loc::getMessage("CH_PAYMENT_TITLE");?>
                        </div>
                        <div class="orderStep__iconWrap">
                            <img class="res-i" src="/static/img/svg/checkout-chevron-down.svg" alt=""/>
                        </div>
                    </div>
                    <div class="form-wrap__radioWrap<?if(!$expandSteps['stepBox_5']):?> collapse<?endif;?>">

                        <?include $documentRoot.$templateFolder.'/payment.php';?>

                    </div>
                    <?foreach ($arResult['fields'] as $field):?>
                        <?if (!empty($field['error'])):?>
                            <div class="link-pink">Не корректно заполненное поле: <?=$field['NAME'];?></div>
                        <?endif;?>
                    <?endforeach;?>
                    <?if(!empty($arResult['errors'])):?>
                        <?foreach ($arResult['errors'] as $error):?>
                            <div class="link-pink">Ошибка: <?=$error;?></div>
                        <?endforeach;?>
                    <?endif;?>
                </div>
                <?include $documentRoot.$templateFolder.'/hiddenFields.php';?>
            </form>
        </div>
    </div>
    <div class="orderButton orderButton--lastbuttons">
        <div class="row">
            <div class="col-sm-auto">
                <button
                        class="orderButton__next btn btn-accent<?if($expandSteps['stepBox_5']):?> order_action<?endif;?>"
                        type="submit"
                        data-action="createOrder"
                        <?if(!$expandSteps['stepBox_5']):?>
                            disabled
                        <?endif;?>
                >
                    <?=Loc::getMessage("CH_CREATE_BUTTON");?>
                </button>
            </div>
            <div class="col-sm-auto">
                <a class="orderButton__back btn" href="/personal/cart/"><?=Loc::getMessage("CH_BASKET_BUTTON");?></a>
            </div>
        </div>
    </div>
</div>
<!--oreder col-->
<div class="sCheckout__col col" data-sticky-container="data-sticky-container">
    <div class="sCheckout__orderWrap" data-sticky="data-sticky" data-margin-top="20">


        <div class="sCheckout__order">
            <?include $documentRoot.$templateFolder.'/total.php';?>
            <?include $documentRoot.$templateFolder.'/basket_items.php';?>
        </div>
        <div class="sCheckout__buttonBlock">
            <a class="sCheckout__backToBascket btn" href="/personal/cart/"><?=Loc::getMessage("CH_BASKET_BUTTON");?></a>
            <button
                    class="sCheckout__done btn btn-accent<?if($expandSteps['stepBox_5']):?> order_action<?endif;?>"
                    type="button"
                    data-action="createOrder"
                    <?if(!$expandSteps['stepBox_5']):?>
                        disabled
                    <?endif;?>
            >
                <?=Loc::getMessage("CH_CREATE_BUTTON");?>
            </button>
        </div>
    </div>
</div>

