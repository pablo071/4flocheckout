<?php

use WS\Components\Checkout;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */
/** @var array $arHeaders */
?>
<?php
$fields = $arResult['fields'];
$settingsFields = $arResult['settingsFields'];
$expandSteps = $arResult['expandSteps'];
$action = false;
if ($expandSteps['stepBox_3'] || $expandSteps['stepBox_4'] || $expandSteps['stepBox_5']) {
    $action = true;
}
?>
<div class="form-wrap__custom-input-wrap row">
    <?foreach ($fields as $code => $field):?>
        <?if (!in_array($code, $settingsFields)) continue;?>
        <div class="col-sm-6 col-md-4">
            <label class="custom-input">
                <input
                        id="ID_<?=$field['CODE'];?>"
                        class="custom-input__input<?if($action && ($code == Checkout::PROPERTY_CODE_ASK_RECIPIENT_ADDRESS)):?> order_action<?endif;?>"
                        type="checkbox"
                        name="<?=$field['FIELD_NAME'];?>"
                        <?if($field['VALUE'] == 'Y'):?>
                            checked
                        <?endif;?>
                        value="Y"
                        <?if($action && ($code == Checkout::PROPERTY_CODE_ASK_RECIPIENT_ADDRESS)):?>
                            data-action="nextStep"
                            data-step="3"
                        <?endif;?>
                />
                <span class="custom-input__lab"></span>
                <span class="custom-input__text"><?=$field['NAME'];?></span>
            </label>
        </div>
    <?endforeach;?>
</div>